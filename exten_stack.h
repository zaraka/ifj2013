/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef EXTENSTACK_H
#define	EXTENSTACK_H


#include "scanner.h"
#include "sstring.h"

#ifdef	__cplusplus
extern "C" {
#endif

    struct SASSItem;
  
    typedef struct SExtenStackData {
        SToken token;
	struct SASSItem *tree;
	int top;
    } SExtenStackData;
  
   typedef struct SExtenStack {
        SExtenStackData *data;
	int top;
    } SExtenStack;
 

  SExtenStack* stackInit();
  
  void stackPrint( SExtenStack* ptrstack);
  
  void stackDestroy(SExtenStack* stack);
  
  void stackPush(SExtenStack* stack, SExtenStackData token);

  SExtenStackData stackTop(SExtenStack* stack);
  
  SExtenStackData stackPop(SExtenStack* stack);
 
  SExtenStackData stackGetFirstTerm(SExtenStack* stack);

  void stackInsertAfterFirstTerm(SExtenStack* stack, SExtenStackData token);
#ifdef	__cplusplus
}
#endif

#endif	/* EXTENSTACK_H */