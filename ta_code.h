/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef TACODE_H
#define	TACODE_H

#include <stdio.h>
#include "sstring.h"

#ifdef	__cplusplus
extern "C" {
#endif

    enum name{
        tac_mul = 0,
	tac_div,
	tac_add,
	tac_sub,
	tac_con,
	tac_ls,
	tac_gr,
	tac_lse,
	tac_gre,
	tac_eq,
	tac_neq,
	tac_push,
	tac_call,
	tac_pop,
	tac_ret,
	tac_goto,
	tac_if,
        tac_mov,
        tac_label
    }ETacName;
    
    //struct of three adress code
    typedef struct STacItem{
        int name;
        SString destination;
        SString paramF;
        SString paramS;
	int label;
    }STacItem;
    
    //struct of table
    typedef struct STacList{
        STacItem *item;
	int size;
    }STacList;
    
    typedef struct STacTable{
        STacList *list;
	int size;
    }STacTable;
    
    

    
    //inicialization of table 
    STacItem STacCreateItem(int name, SString destination, SString paramF, SString paramS, int label);
    
    STacTable STacTableInit();
    STacList STacListInit();
    //dealocation table & set table to NULL
    void STacTableDestroy(STacTable *table);
    void STacListDestroy(STacList *list);
    //insert new member to table 
    int STacItemInsert(STacList *list, STacItem item);
    int STacListInsert(STacTable *table, STacList list);
    
    int STacFindLabel(STacList *list, int label, int actualItem);
    
    int STacPrintTable(STacTable *table);
    
    char* STacGetItemName(int name);
    
#ifdef	__cplusplus
}
#endif

#endif	/* TACODE_H */

