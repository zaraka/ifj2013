/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef TERM_H
#define	TERM_H

#include <stdbool.h>
#include "sstring.h"

#ifdef	__cplusplus
extern "C" {
#endif
    

    enum DATA_TYPE {
        INT = 0,
        REAL = 1,
        STRING = 2,
        NIL = 3,
        EMPTY = 4,
        BOOL = 5,
        FUNCTION
    };

    /*how to define null?
     * solved its nil!*/
    
    typedef union Data {
        int integer;
        double real;
        SString string;
        bool boolean;
    }Data;

    
    /**
     * data union that stores data
     * type what type of data is stored
     */
    typedef struct STerm {
        Data data;
        int type;
    } STerm;
    
    /**
     * 
     * OMG kdo tohle implementoval by měl dostat facku a nejlépe tak deset, 
     * tohle je tak moc špatně on so many levels, můj ty bože
     * Opraveno Nikitou
     * 
     * P.S: SAKRA, VŽDYŤ TOHLE MĚLA BÝT NEJLEHČÍ KNIHOVNA Z CELÉHO PARSERU
     * 
     */
    
    /**
     * will cleanup STerm
     */
    void destroyTerm(STerm *term);
    
    bool boolval(STerm *term);
    
    double doubleval(STerm *term);
    
    int intval(STerm *term);
    
    SString strval(STerm *term);
    
    char* getTermType(STerm *term);
    
    void printTerm(STerm *term);

#ifdef	__cplusplus
}
#endif

#endif	/* TERM_H */

