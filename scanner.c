/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "scanner.h"
#include "sstring.h"
#include "error_codes.h"

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include <string.h>

FILE *_file;
int _row, _column;

#define KEYWORDS_NUM 7
char *KEYWORDS[KEYWORDS_NUM] = {
    "for", "if", "else", "while", "function", "do", "return"
};

SToken tokenInit(int type, char* data, int row, int column) {
    SToken token;
    token.type = type;
    if (data != NULL) {
        token.data = stringInit(data);
    } else {
        token.data.string = NULL;
        token.data.stringSize = 0;
    }
    token.row = row;
    token.column = column;
    return token;
}

void tokenDestroy(SToken token) {
    if (token.data.string != NULL) {
        stringDestroy(&token.data);
    }
}

SToken getToken() {
    SToken resultToken;
    resultToken.column = 0;
    resultToken.row = 0;
    resultToken.type = LEX_START;
    resultToken.data.string = NULL;
    resultToken.data.stringSize = 0;

    bool loadNext = true;
    int character;
    
    while (loadNext) {
        character = fgetc(_file);
        char ch_character = (char) character;
        bool newline = false;
#ifdef DEBUG_SCANNER
        printf("\nscanning %c\n", character);
#endif        
        switch (resultToken.type) {
            case LEX_START:
                switch (character) {
                    case '+':
                        resultToken.type = LEX_PLUS;
                        break;
                    case '-':
                        resultToken.type = LEX_MINUS;
                        break;
                    case '*':
                        resultToken.type = LEX_MUL;
                        break;
                    case '/':
                        resultToken.type = LEX_DIV;
                        break;
                    case '%':
                        resultToken.type = LEX_MODULO;
                        break;
                    case '=':
                        resultToken.type = LEX_ASSIGN;
                        break;
                    case '!':
                        resultToken.type = LEX_NOT;
                        break;
                    case '$':
                        resultToken.type = LEX_IDENT_START;
                        break;
                    case '<':
                        resultToken.type = LEX_LESS_THAN;
                        break;
                    case '>':
                        resultToken.type = LEX_MORE_THAN;
                        break;
                    case '?':
                        resultToken.type = LEX_QUESTION;
                        break;
                    case EOF:
                        resultToken.type = LEX_ENDOF;
                        loadNext = false;
                        break;
                    case ';':
                        resultToken.type = LEX_SEMICOLON;
                        loadNext = false;
                        break;
                    case '(':
                        resultToken.type = LEX_BRACKET_R_L;
                        loadNext = false;
                        break;
                    case ')':
                        resultToken.type = LEX_BRACKET_R_R;
                        loadNext = false;
                        break;
                    case '{':
                        resultToken.type = LEX_BRACKET_C_L;
                        loadNext = false;
                        break;
                    case '}':
                        resultToken.type = LEX_BRACKET_C_R;
                        loadNext = false;
                        break;
                    case '[':
                        resultToken.type = LEX_BRACKET_S_L;
                        loadNext = false;
                        break;
                    case ']':
                        resultToken.type = LEX_BRACKET_S_R;
                        loadNext = false;
                        break;
                    case '"':
                        resultToken.type = LEX_STRING_START;
                        char ch = '\0';
                        resultToken.data = stringInit(&ch);
                        break;
                    case ',':
                        resultToken.type = LEX_COMMA;
                        loadNext = false;
                        break;
                    case '.':
                        resultToken.type = LEX_DOT;
                        loadNext = false;
                        break;
                    default:
                        if (isalpha(character)) {
                            //can be a KEYWORD
                            resultToken.type = LEX_WORD_START;
                            char ch = '\0';
                            resultToken.data = stringInit(&ch);
                            stringAddChar(&resultToken.data, ch_character);
                        } else if (isdigit(character)) {
                            resultToken.type = LEX_INT;
                            char ch = '\0';
                            resultToken.data = stringInit(&ch);
                            stringAddChar(&resultToken.data, ch_character);
                        } else if (isspace(character)) {
                            if (character == '\n') {
                                newline = true;
                            }
                        } else {
                            resultToken.type = LEX_ERROR;
                            loadNext = false;
                        }
                        break;
                }
                break;
            case LEX_STRING_START:
                if (character == '$'){
                    //do nothing?
                } else if (character == '\\') {
                    resultToken.type = LEX_BACKSLASH;
                } else if (character != '"') {
                    stringAddChar(&resultToken.data, ch_character);
                } else if (character == '"') {
                    resultToken.type = LEX_STRING;
                    loadNext = false;
                }
                break;
            case LEX_BACKSLASH:
                switch (character) {
                    case 'n':
                        stringAddChar(&resultToken.data, '\n');
                        resultToken.type = LEX_STRING_START;
                        break;
                    case 't':
                        stringAddChar(&resultToken.data, '\t');
                        resultToken.type = LEX_STRING_START;
                        break;
                    case '\\':
                        stringAddChar(&resultToken.data, '\\');
                        resultToken.type = LEX_STRING_START;
                        break;
                    case '"':
                        stringAddChar(&resultToken.data, '"');
                        resultToken.type = LEX_STRING_START;
                        break;
                    case '$':
                        stringAddChar(&resultToken.data, '$');
                        resultToken.type = LEX_STRING_START;
                        break;
                    case 'x':
                        resultToken.type = LEX_STRING_HEX_START;
                        break;
                    default:
                        stringAddChar(&resultToken.data, character);
                        resultToken.type = LEX_STRING_START;
                        break;
                }
                break;
            case LEX_STRING_HEX_START:
                if(isxdigit(character)){
                    stringAddChar(&resultToken.data, ch_character);
                    resultToken.type = LEX_STRING_HEX;
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                    resultToken.type = LEX_ERROR;
                }
                break;
            case LEX_STRING_HEX:
                if(isxdigit(character)){
                    char hex[] = {resultToken.data.string[resultToken.data.stringSize - 1], ch_character, '\0'};
                    stringRemoveChar(&resultToken.data);
                    int number = (int)strtol(hex, NULL, 16);
                    sprintf(hex, "%c", number);
                    stringAddChar(&resultToken.data, hex[0]);
                    resultToken.type = LEX_STRING_START;
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                    resultToken.type = LEX_ERROR;
                }
                break;
            case LEX_ASSIGN:
                switch (character) {
                    case '=':
                        resultToken.type = LEX_EQUAL_START;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_EQUAL_START:
                if(character == '='){
                    resultToken.type = LEX_EQUAL;
                    loadNext = false;
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                    resultToken.type = LEX_ERROR;
                }
                break;
            case LEX_PLUS:
                switch (character) {
                    case '+':
                        resultToken.type = LEX_INC;
                        loadNext = false;
                        break;
                    case '=':
                        resultToken.type = LEX_ASSIGN_PLUS;
                        loadNext = false;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_MINUS:
                switch (character) {
                    case '-':
                        resultToken.type = LEX_DEC;
                        loadNext = false;
                        break;
                    case '=':
                        resultToken.type = LEX_ASSIGN_MINUS;
                        loadNext = false;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_MUL:
                switch (character) {
                    case '=':
                        resultToken.type = LEX_ASSIGN_MUL;
                        loadNext = false;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_DIV:
                switch (character) {
                    case '=':
                        resultToken.type = LEX_ASSIGN_DIV;
                        loadNext = false;
                        break;
                    case '/':
                        resultToken.type = LEX_COMMENT_SIMPLE;
                        break;
                    case '*':
                        resultToken.type = LEX_COMMENT_COMPLEX;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_NOT:
                switch (character) {
                    case '=':
                        resultToken.type = LEX_NOT_EQUAL_START;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_NOT_EQUAL_START:
                if(character == '='){
                    resultToken.type = LEX_NOT_EQUAL;
                    loadNext = false;
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                    resultToken.type = LEX_ERROR;
                }
                break;
            case LEX_LESS_THAN:
                switch (character) {
                    case '=':
                        resultToken.type = LEX_LESS_E_THAN;
                        loadNext = false;
                        break;
                    case '?':
                        resultToken.type = LEX_PHP_START1;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_PHP_START1:
                switch (character) {
                    case 'p':
                        resultToken.type = LEX_PHP_START2;
                        break;
                    default:
                        resultToken.type = LEX_ERROR;
                        loadNext = false;
                        break;
                }
                break;
            case LEX_PHP_START2:
                switch (character) {
                    case 'h':
                        resultToken.type = LEX_PHP_START3;
                        break;
                    default:
                        resultToken.type = LEX_ERROR;
                        loadNext = false;
                        break;
                }
                break;
            case LEX_PHP_START3:
                switch (character) {
                    case 'p':
                        resultToken.type = LEX_PHP_START;
                        loadNext = false;
                        break;
                    default:
                        resultToken.type = LEX_ERROR;
                        loadNext = false;
                        break;
                }
                break;
            case LEX_MORE_THAN:
                switch (character) {
                    case '=':
                        resultToken.type = LEX_MORE_E_THAN;
                        loadNext = false;
                        break;
                    default:
                        ungetc(character, _file);
                        loadNext = false;
                        break;
                }
                break;
            case LEX_IDENT_START:
                if (isalpha(character) || character == '_') {
                    resultToken.type = LEX_IDENT;
                    char ch = '\0';
                    resultToken.data = stringInit(&ch);
                    stringAddChar(&resultToken.data, ch_character);
                } else {
                    resultToken.type = LEX_ERROR;
                    loadNext = false;
                }
                break;
            case LEX_IDENT:
                if (isalnum(character) || character == '_') {
                    stringAddChar(&resultToken.data, ch_character);
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                }
                break;
            case LEX_WORD_START:
                if (isalpha(character) || character == '_') {
                    stringAddChar(&resultToken.data, ch_character);
                } else {
                    //check for keywords
                    for (int i = 0; i < KEYWORDS_NUM; i++) {
                        if (strcmp(KEYWORDS[i], resultToken.data.string) == 0) {
                            resultToken.type = LEX_KEYWORD;
                            break;
                        }
                    }

                    if (strcmp("true", resultToken.data.string) == 0 || strcmp("false", resultToken.data.string) == 0) {
                        resultToken.type = LEX_BOOL;
                    }

                    if (strcmp("null", resultToken.data.string) == 0) {
                        resultToken.type = LEX_NULL;
                        stringDestroy(&resultToken.data);
                    }

                    if (resultToken.type == LEX_WORD_START) {
                        resultToken.type = LEX_WORD;
                    }

                    ungetc(character, _file);
                    loadNext = false;
                }
                break;
            case LEX_INT:
                if (isdigit(character)) {
                    stringAddChar(&resultToken.data, ch_character);
                } else if (character == '.') {
                    resultToken.type = LEX_DOUBLE;
                    stringAddChar(&resultToken.data, ch_character);
                } else if(character == 'e' || character == 'E') {
                    stringAddChar(&resultToken.data, ch_character);
                    resultToken.type = LEX_DOUBLE_POW_START;
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                }
                break;
            case LEX_DOUBLE:
                if (isdigit(character)) {
                    stringAddChar(&resultToken.data, ch_character);
                } else if(character == 'e' || character == 'E'){
                    stringAddChar(&resultToken.data, ch_character);
                    resultToken.type = LEX_DOUBLE_POW_START;
                }else {
                    ungetc(character, _file);
                    loadNext = false;
                }
                break;
            case LEX_DOUBLE_POW_START:
                if(character == '-' || character == '+' || isdigit(character)){
                    resultToken.type = LEX_DOUBLE_POW;
                    stringAddChar(&resultToken.data, ch_character);
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                    resultToken.type = LEX_ERROR;
                }
                break;
            case LEX_DOUBLE_POW:
                if(isdigit(character)){
                    stringAddChar(&resultToken.data, ch_character);
                } else {
                    ungetc(character, _file);
                    loadNext = false;
                    resultToken.type = LEX_DOUBLE;
                }
                break;
            case LEX_QUESTION:
                switch (character) {
                    case '>':
                        resultToken.type = LEX_PHP_END;
                        loadNext = false;
                        break;
                    default:
                        resultToken.type = LEX_ERROR;
                        ungetc(character, _file);
                        loadNext = false;
                }
                break;
            case LEX_COMMENT_SIMPLE:
                if (character == '\n') {
                    loadNext = false;
                }
                break;
            case LEX_COMMENT_COMPLEX:
                if (character == '*') {
                    resultToken.type = LEX_COMMENT_COMPLEX_END;
                } else if (character == EOF){
                    loadNext = false;
                    ungetc(character, _file);
                    fprintf(stderr, "WARNING: Unterminated comment!\n");
                }
                break;
            case LEX_COMMENT_COMPLEX_END:
                if (character == '/') {
                    resultToken.type = LEX_COMMENT_COMPLEX;
                    loadNext = false;
                } else {
                    resultToken.type = LEX_COMMENT_COMPLEX;
                }
                break;
            default:
                resultToken.type = LEX_ERROR;
                loadNext = false;
        }

        resultToken.row = _row;
        resultToken.column = _column;

        //lets update global vars and tokens data
        if (newline) {
            _row++;
            _column = 0;
        } else {
            _column++;
        }
    }
    
    if(resultToken.type == LEX_ERROR){
        fprintf(stderr, "LEX ERROR: Unexpected character '%c'\n", character);
    }

    return resultToken;
}

bool initScanner(const char* filename) {
    _file = fopen(filename, "r");
    if (_file == NULL) {
        fprintf(stderr, "File not found or cannot be opened\n");
        return false;
    }

#ifdef DEBUG
    printf("file %s opened!\n", filename);
#endif
    _row = 0;
    _column = 0;
    return true;
}

void destroyScanner() {
    fclose(_file);
}

/**
 * Will return STRING! representation of Token
 * @param type of token!
 * @return string
 * @note EXPANDED tokens are included (but not used)
 */
char* getTokenType(int type) {
    switch (type) {
        case LEX_ASSIGN:
            return "=";
            break;
        case LEX_ASSIGN_DIV:
            return "/=";
            break;
        case LEX_ASSIGN_MINUS:
            return "-=";
            break;
        case LEX_ASSIGN_MUL:
            return "*=";
            break;
        case LEX_ASSIGN_PLUS:
            return "+=";
            break;
        case LEX_BRACKET_C_L:
            return "{";
            break;
        case LEX_BRACKET_C_R:
            return "}";
            break;
        case LEX_BRACKET_R_L:
            return "(";
            break;
        case LEX_BRACKET_R_R:
            return ")";
            break;
        case LEX_BRACKET_S_L:
            return "[";
            break;
        case LEX_BRACKET_S_R:
            return "]";
            break;
        case LEX_COMMA:
            return ",";
            break;
        case LEX_DEC:
            return "--";
            break;
        case LEX_DIV:
            return "/";
            break;
        case LEX_DOUBLE:
            return "double";
            break;
        case LEX_END:
            return "end";
            break;
        case LEX_ENDOF:
            return "EOF";
            break;
        case LEX_ERROR:
            return "ERROR";
            break;
        case LEX_ERROR_INTER:
            return "ERROR INTER";
            break;
        case LEX_IDENT:
            return "IDENT";
            break;
        case LEX_IDENT_START:
            return "IDENT start";
            break;
        case LEX_INC:
            return "++";
            break;
        case LEX_KEYWORD:
            return "keyword";
            break;
        case LEX_LESS_E_THAN:
            return "<=";
            break;
        case LEX_LESS_THAN:
            return "<";
            break;
        case LEX_MINUS:
            return "-";
            break;
        case LEX_MODULO:
            return "%";
            break;
        case LEX_MORE_E_THAN:
            return ">=";
            break;
        case LEX_MORE_THAN:
            return ">";
            break;
        case LEX_MUL:
            return "*";
            break;
        case LEX_NEWLINE:
            return "newline";
            break;
        case LEX_NOT:
            return "!";
            break;
        case LEX_NOT_EQUAL:
            return "!==";
            break;
        case LEX_PHP_END:
            return "?>";
            break;
        case LEX_PHP_START:
            return "<?php";
            break;
        case LEX_PHP_START1:
            return "<?";
            break;
        case LEX_PHP_START2:
            return "<?p";
            break;
        case LEX_PHP_START3:
            return "<?ph";
            break;
        case LEX_PLUS:
            return "+";
            break;
        case LEX_QUESTION:
            return "?";
            break;
        case LEX_SEMICOLON:
            return ";";
            break;
        case LEX_START:
            return "START";
            break;
        case LEX_STRING:
            return "string";
            break;
        case LEX_WORD_START:
            return "word start(error)";
            break;
        case LEX_WORD:
            return "word";
            break;
        case LEX_INT:
            return "integer";
            break;
        case LEX_BOOL:
            return "boolean";
            break;
        case LEX_NULL:
            return "null";
            break;
        case NON_TERM:
            return "E";
            break;
        case LEX_OPENER:
            return "opener";
            break;
            case LEX_BEFORE_AFTER:
            return "BA";
            break;
        case LEX_FUNCTION:
            return "FUNC";
            break;
        case LEX_BIT_LOGIC:
            return "BL";
            break;
        case LEX_FUNCTION_CALL:
            return "CALL";
            break;
        case LEX_IF:
            return "IF";
            break;
        case LEX_WHILE:
            return "WHILE";
            break;
        case LEX_EQUAL:
            return "===";
            break;
        case LEX_RETURN:
            return "RETURN";
            break;
        default:
            return "?";
            break;
    }
}

void printToken(SToken token) {
    if (token.type == LEX_INT || token.type == LEX_DOUBLE || token.type == LEX_STRING || token.type == LEX_IDENT || token.type == LEX_KEYWORD || token.type == LEX_WORD) {
        printf("TYPE %s DATA SIZE %d DATA %s ROW %d COLUMN %d\n", getTokenType(token.type), token.data.stringSize, token.data.string, token.row, token.column);
        for (int i = 0; i < stringStrlen(token.data); i++) {
            printf("'%c' ", token.data.string[i]);
        }
        if (token.data.string[stringStrlen(token.data)] != '\0') {
            printf("String not ended!\n");
        }
        printf("\n");
    } else {
        printf("TYPE %s ROW %d COLUMN %d\n", getTokenType(token.type), token.row, token.column);
    }
}