/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdio.h>
#include <stdlib.h>
#include "taCode.h"
#include "error_codes.h"
#include "sstring.h"

/*
 * 
 */
int main() {
    int name = 0;
    char *a = "A";
    SString destination = stringInit(a);
    a = "B";
    SString paramF = stringInit(a);
    a = "C";
    SString paramS = stringInit(a);
    int label = 0;
    
    printf("vytvorenie itemu\n");
    STacItem item = STacCreateItem(name,  destination, paramF, paramS, label);
    printf("%d %s %s %s %d\n=========================\n",item.name,item.destination.string,item.paramF.string,item.paramS.string,item.label);
    
    printf("vytvorenie sTacTable\n");
    STacTable table = STacTableInit();
    printf("%d\n=========================\n",table.size);
    
    printf("vytvorenie sTacList\n");
    STacList list = STacList_Init();
    printf("%d\n=========================\n",list.size);
    
    printf("STacItemInsert\n");
    int insert1 = STacItemInsert(&list, item);
    printf("%d = list size, insert = ",list.size);
    printf("%d\n=========================\n",insert1);
   
    printf("STacListInsert\n");
    int insert2 = STacListInsert(&table, list);
    printf("%d = table size, insert = ",table.size);
    printf("%d\n=========================\n",insert2);
    
    printf("STacFindLabel\n");
    int actualItem = 0;
    int find = STacFindLabel(&list, label,actualItem);
    printf("%d\n=========================\n",find);
    
    printf("STacTableDestroy\n");
    STacTableDestroy(&table);
    printf("%d = table size \n===================\n",table.size);
   
    printf("STacListDestroy\n");
    STacListDestroy(&list);
    printf("%d = list size\n===================\n",list.size);
       
    return 0;
}

