/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "interpret.h"
#include <stdlib.h>
#include "error_codes.h" 


#define NILL -1
#define sizeOftermStackBlocks 128
#define NOT_FOUND -1

int interStart(STacTable *tacTable, SMapTable * mapTable, SHashtTable *function, int *returnCode){
  int actualTable = 0, error;
  STermStack *stack;
  
  stack = termStackInit();
  if(stack == NULL)
    return ERR_INTERNAL;
  

  
  error = interRekRun(tacTable, mapTable, mapTable, function, actualTable, stack, returnCode);
  
  termStackDestroy(stack);
  
  return error;
}

int interRekRun(STacTable *tacTable, SMapTable * mapTable, SMapTable * originalTable, SHashtTable *function, int actualTable, STermStack *stack, int *returnCode){
  int ip = 0, error;
  SHTableItem *destination, *paramF;
  int lastTable;
  char buffer[32];
  SHTableItem *tableItem;
  
  sprintf(buffer, "%d", NILL);
  tableItem = htableLookup(mapTable->tables[0], buffer);
  tableItem->data.type = NIL;  
  
  while(ip < tacTable->list[actualTable].size){
      if (tacTable->list[actualTable].item[ip].name <= tac_neq){
	  error = interDoOperation( &(tacTable->list[actualTable].item[ip]), mapTable->tables[0]);
	  if(error != 0 )
	      return error;
	  
      }else if (tacTable->list[actualTable].item[ip].name == tac_push){
	  paramF = htableFind(mapTable->tables[0], tacTable->list[actualTable].item[ip].paramF.string);
          if(paramF == NULL)
                return ERR_NOT_DECLARED;          
	  termStackPush(stack, paramF->data);
	  
      }else if (tacTable->list[actualTable].item[ip].name == tac_pop){
	  destination = htableLookup(mapTable->tables[0], tacTable->list[actualTable].item[ip].destination.string);
	  destination->data = termStackPop(stack);
	  
      }else if (tacTable->list[actualTable].item[ip].name == tac_goto){ 
	  ip = STacFindLabel(&(tacTable->list[actualTable]), strtol(tacTable->list[actualTable].item[ip].paramF.string, NULL, 10), ip);
	  
      }else if (tacTable->list[actualTable].item[ip].name == tac_if){
	  paramF = htableFind(mapTable->tables[0], tacTable->list[actualTable].item[ip].paramF.string);
          if(paramF == NULL)
                return ERR_NOT_DECLARED;
	  if(boolval(&(paramF->data)) == false){
	      ip = STacFindLabel( &(tacTable->list[actualTable]), strtol(tacTable->list[actualTable].item[ip].paramS.string, NULL, 10), ip);
	  }
	  
      }else if (tacTable->list[actualTable].item[ip].name == tac_call){ 
          if (checkInBuildFunction(&(tacTable->list[actualTable].item[ip]), mapTable->tables[0], stack) == NOT_FOUND){
          
	  paramF = htableFind(function, tacTable->list[actualTable].item[ip].paramF.string);
          if(paramF == NULL)
                return ERR_NOT_DECLARED;
	  if(tacTable->list[actualTable].item[ip].destination.string != NULL )
	    destination = htableLookup(mapTable->tables[0], tacTable->list[actualTable].item[ip].destination.string);
	  
          
          SMapTable *subtable;
          subtable = initSMap();
          insertSMap(subtable);
          //functionTable->tables[0] = memcpy(functionTable->tables[0], originalTable->tables[paramF->data.data.integer], mapTable->tables[paramF->data.data.integer]->htable_size * sizeof(SHashtTable) + mapTable->tables[paramF->data.data.integer]->htable_size * sizeof(SHTableItem));        
          subtable->tables[0] = htableCopy(originalTable->tables[paramF->data.data.integer]);

          lastTable = actualTable;
          actualTable = paramF->data.data.integer;
          
	  error = interRekRun(tacTable, subtable, originalTable, function, actualTable, stack, returnCode);
	  if(error != 0)
	    return error;
	  
          destroySMap(subtable);

          actualTable = lastTable;
          
	  if(tacTable->list[actualTable].item[ip].destination.string != NULL )
	    destination->data = termStackPop(stack);
	  else
	    termStackPop(stack);
	  
          }
          
      }else if (tacTable->list[actualTable].item[ip].name == tac_ret){
          if(tacTable->list[actualTable].item[ip].paramF.string != NULL){
                paramF = htableFind(mapTable->tables[0], tacTable->list[actualTable].item[ip].paramF.string);
                if(paramF == NULL)
                        return ERR_NOT_DECLARED;              
          }
          else{
                    sprintf(buffer, "%d", NILL);
                    paramF = htableLookup(mapTable->tables[0], buffer);
                    paramF->data.type = NIL;
          }
              
          

	  
          if(actualTable == 0)
              *returnCode = intval(&(paramF->data));
          else
              termStackPush(stack, paramF->data);
	  
	  return 0;	
      }else if (tacTable->list[actualTable].item[ip].name == tac_mov){
	  paramF = htableFind(mapTable->tables[0], tacTable->list[actualTable].item[ip].paramF.string);
          if(paramF == NULL)
                return ERR_NOT_DECLARED;          
          destination = htableLookup(mapTable->tables[0], tacTable->list[actualTable].item[ip].destination.string);
          
          destination->data.type = paramF->data.type;
          destination->data.data = paramF->data.data;
          
      //}else if (tacTable->list[actualTable].item[ip].name == tac_label){
          
      }
      
      
      ip++;
  }
  
  return 0;
}


int interDoOperation(STacItem *item, SHashtTable *table){
     SHTableItem *destination, *paramF, *paramS;
    
     paramF = htableFind(table, item->paramF.string);
     if(paramF == NULL)
         return ERR_NOT_DECLARED;
     paramS = htableFind(table, item->paramS.string); 
     if(paramS == NULL)
         return ERR_NOT_DECLARED;
     
     destination = htableLookup(table, item->destination.string);
        
    switch(item->name){
	case tac_mul:
	    if(paramF->data.type == INT && paramS->data.type == INT){ 
                destination->data.type = INT;
		destination->data.data.integer = paramF->data.data.integer * paramS->data.data.integer;
	    
	    }else if(paramF->data.type == REAL && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real * paramS->data.data.real;
	      
	    }else if(paramF->data.type == REAL && paramS->data.type == INT){ 
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real * doubleval(&(paramS->data));
	    
	    }else if(paramF->data.type == INT && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = doubleval(&(paramF->data)) * paramS->data.data.real;
	      
	    }else{
	      return ERR_ARR;
	    }	 
	  break;
	  
	case tac_div: 
	    if (paramS->data.type == INT && paramS->data.data.integer == 0)
	      return ERR_DIVIDE_ZERO;
	    else if(paramS->data.type == REAL && paramS->data.data.real == 0.0)
	      return ERR_DIVIDE_ZERO;
	      
	    if(paramF->data.type == INT && paramS->data.type == INT){ 
                destination->data.type = INT;
		destination->data.data.integer = paramF->data.data.integer / paramS->data.data.integer;
	    
	    }else if(paramF->data.type == REAL && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real / paramS->data.data.real;
	      
	    }else if(paramF->data.type == REAL && paramS->data.type == INT){ 
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real / doubleval(&(paramS->data));
	    
	    }else if(paramF->data.type == INT && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = doubleval(&(paramF->data)) / paramS->data.data.real;
	      
	    }else{
	      return ERR_ARR;
	    }	  
	  break;
	  
	case tac_add: 
	    if(paramF->data.type == INT && paramS->data.type == INT){ 
                destination->data.type = INT;
		destination->data.data.integer = paramF->data.data.integer + paramS->data.data.integer;
	    
	    }else if(paramF->data.type == REAL && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real + paramS->data.data.real;
	      
	    }else if(paramF->data.type == REAL && paramS->data.type == INT){ 
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real + doubleval(&(paramS->data));
	    
	    }else if(paramF->data.type == INT && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = doubleval(&(paramF->data)) + paramS->data.data.real;
	      
	    }else{
	      return ERR_ARR;
	    }	   
	  break;
	  
	case tac_sub:
	    if(paramF->data.type == INT && paramS->data.type == INT){ 
                destination->data.type = INT;
		destination->data.data.integer = paramF->data.data.integer - paramS->data.data.integer;
	    
	    }else if(paramF->data.type == REAL && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real - paramS->data.data.real;
	      
	    }else if(paramF->data.type == REAL && paramS->data.type == INT){ 
                destination->data.type = REAL;
                destination->data.data.real = paramF->data.data.real - doubleval(&(paramS->data));
	    
	    }else if(paramF->data.type == INT && paramS->data.type == REAL){
                destination->data.type = REAL;
                destination->data.data.real = doubleval(&(paramF->data)) - paramS->data.data.real;
	      
	    }else{
                return ERR_ARR;
	    } 
	  break;
	  
	case tac_con:
	    if(paramF->data.type == STRING || paramS->data.type == STRING){
                destination->data.type = STRING;
                destination->data.data.string = concate_string(strval(&(paramF->data)), strval(&(paramS->data)));
	    }else{
	      return ERR_ARR;
	    }	  	  
	  break;
	  
	case tac_ls:
                  destination->data.type = BOOL;
		  if(paramF->data.type == INT && paramS->data.type == INT){
			destination->data.data.boolean = paramF->data.data.integer < paramS->data.data.integer;
		      
		  }else if(paramF->data.type == REAL && paramS->data.type == REAL){
			destination->data.data.boolean = paramF->data.data.integer < paramS->data.data.real;
		       
		  }else if(paramF->data.type == STRING && paramS->data.type == STRING){
			if(compare_strings(paramF->data.data.string, paramS->data.data.string) < 0)
			    destination->data.data.boolean = true;
			else
			    destination->data.data.boolean = false;
		       
		  }else if(paramF->data.type == NIL && paramS->data.type == NIL){
			destination->data.data.boolean = false;
			
		  }else if(paramF->data.type == BOOL && paramS->data.type == BOOL){
			if (paramF->data.data.boolean == false && paramS->data.data.boolean == true)
			    destination->data.data.boolean = true;  
			else
			    destination->data.data.boolean = false;
			  
		  }else{
		    return ERR_ARR;
		  }
	  break;
	case tac_gr: 
                  destination->data.type = BOOL;            
		  if(paramF->data.type == INT && paramS->data.type == INT){
			destination->data.data.boolean = paramF->data.data.integer > paramS->data.data.integer;
		      
		  }else if(paramF->data.type == REAL && paramS->data.type == REAL){
			destination->data.data.boolean = paramF->data.data.integer > paramS->data.data.real;
		       
		  }else if(paramF->data.type == STRING && paramS->data.type == STRING){
			if(compare_strings(paramF->data.data.string, paramS->data.data.string) > 0)
			    destination->data.data.boolean = true;
			else
			    destination->data.data.boolean = false;
		       
		  }else if(paramF->data.type == NIL && paramS->data.type == NIL){
			destination->data.data.boolean = false;
			
		  }else if(paramF->data.type == BOOL && paramS->data.type == BOOL){
			if (paramF->data.data.boolean == true && paramS->data.data.boolean == false)
			    destination->data.data.boolean = true;  
			else
			    destination->data.data.boolean = false;
			  
		  }else{
		    return ERR_ARR;
		  }  
	  break;
	case tac_lse: 
                  destination->data.type = BOOL;            
		  if(paramF->data.type == INT && paramS->data.type == INT){
			destination->data.data.boolean = paramF->data.data.integer <= paramS->data.data.integer;
		      
		  }else if(paramF->data.type == REAL && paramS->data.type == REAL){
			destination->data.data.boolean = paramF->data.data.integer <= paramS->data.data.real;
		       
		  }else if(paramF->data.type == STRING && paramS->data.type == STRING){
			if(compare_strings(paramF->data.data.string, paramS->data.data.string) <= 0)
			    destination->data.data.boolean = true;
			else
			    destination->data.data.boolean = false;
		       
		  }else if(paramF->data.type == NIL && paramS->data.type == NIL){
			destination->data.data.boolean = true;
			
		  }else if(paramF->data.type == BOOL && paramS->data.type == BOOL){
			if (paramF->data.data.boolean == true && paramS->data.data.boolean == false)
			    destination->data.data.boolean = false;  
			else
			    destination->data.data.boolean = true;
			  
		  }else{
		    return ERR_ARR;
		  }  
	  break;
	case tac_gre: 
                  destination->data.type = BOOL;            
		  if(paramF->data.type == INT && paramS->data.type == INT){
			destination->data.data.boolean = paramF->data.data.integer >= paramS->data.data.integer;
		      
		  }else if(paramF->data.type == REAL && paramS->data.type == REAL){
			destination->data.data.boolean = paramF->data.data.integer >= paramS->data.data.real;
		       
		  }else if(paramF->data.type == STRING && paramS->data.type == STRING){
			if(compare_strings(paramF->data.data.string, paramS->data.data.string) >= 0)
			    destination->data.data.boolean = true;
			else
			    destination->data.data.boolean = false;
		       
		  }else if(paramF->data.type == NIL && paramS->data.type == NIL){
			destination->data.data.boolean = true;
			
		  }else if(paramF->data.type == BOOL && paramS->data.type == BOOL){
			if (paramF->data.data.boolean == false && paramS->data.data.boolean == true)
			    destination->data.data.boolean = false;  
			else
			    destination->data.data.boolean = true;
			  
		  }else{
		    return ERR_ARR;
		  }  	  
	  break;
	case tac_eq: 
                  destination->data.type = BOOL;            
		  if(paramF->data.type == INT && paramS->data.type == INT){
			destination->data.data.boolean = paramF->data.data.integer == paramS->data.data.integer;
		      
		  }else if(paramF->data.type == REAL && paramS->data.type == REAL){
			destination->data.data.boolean = paramF->data.data.integer == paramS->data.data.real;
		       
		  }else if(paramF->data.type == STRING && paramS->data.type == STRING){
			if(compare_strings(paramF->data.data.string, paramS->data.data.string) == 0)
			    destination->data.data.boolean = true;
			else
			    destination->data.data.boolean = false;
		       
		  }else if(paramF->data.type == NIL && paramS->data.type == NIL){
			destination->data.data.boolean = true;
			
		  }else if(paramF->data.type == BOOL && paramS->data.type == BOOL){
			if (paramF->data.data.boolean == paramS->data.data.boolean)
			    destination->data.data.boolean = true;  
			else
			    destination->data.data.boolean = false;
			  
		  }else{
		    destination->data.data.boolean = false;
		  } 	  
	  break;
	case tac_neq: 
                  destination->data.type = BOOL;            
		  if(paramF->data.type == INT && paramS->data.type == INT){
			destination->data.data.boolean = paramF->data.data.integer != paramS->data.data.integer;
		      
		  }else if(paramF->data.type == REAL && paramS->data.type == REAL){
			destination->data.data.boolean = paramF->data.data.integer != paramS->data.data.real;
		       
		  }else if(paramF->data.type == STRING && paramS->data.type == STRING){
			if(compare_strings(paramF->data.data.string, paramS->data.data.string) == 0)
			    destination->data.data.boolean = false;
			else
			    destination->data.data.boolean = true;
		       
		  }else if(paramF->data.type == NIL && paramS->data.type == NIL){
			destination->data.data.boolean = false;
			
		  }else if(paramF->data.type == BOOL && paramS->data.type == BOOL){
			if (paramF->data.data.boolean == paramS->data.data.boolean)
			    destination->data.data.boolean = false;  
			else
			    destination->data.data.boolean = true;
			  
		  }else{
		    destination->data.data.boolean = true;
		  } 	  	  
	  break;
    }
    return 0;
}


STermStack* termStackInit(){
  STermStack *stack;

  stack = (STermStack*) malloc(sizeof(STermStack));
  if(stack == NULL)
    return NULL;
  
  stack->data = (STerm*) malloc(sizeof(STerm) * sizeOftermStackBlocks);
  if(stack->data == NULL){
    free(stack);
    return NULL;
  }
  
  stack->size = -1;
  return stack;
}
	
void termStackDestroy(STermStack *stack){
  free(stack->data);
  free(stack);
}
	
STerm termStackPop(STermStack *stack){
  return stack->data[stack->size--];
}
	
int termStackPush(STermStack *stack, STerm term){
  stack->size++;
  
  if(stack->size % (sizeOftermStackBlocks) == 0){
    stack->data = (STerm*) realloc(stack->data, sizeof(STerm) * sizeOftermStackBlocks * (1 + (stack->size / sizeOftermStackBlocks)));
    if(stack->data == NULL){
      free(stack);
      return ERR_INTERNAL;
    }
  }
  
  stack->data[stack->size] = term;
  return 0;
}

int checkInBuildFunction(STacItem *item, SHashtTable *table, STermStack *stack){
  SHTableItem *destination;
  SHTableItem forNull;
  STerm param, param2, param3;
  int error = 0;
  
  //forNull
  
  //item->paramF;
  if(item->destination.string != NULL){
      destination = htableLookup(table, item->destination.string);
  } else {
      destination = &forNull;
  }
      
      if(strcmp(item->paramF.string, "boolval") == 0){	//boolval(term)
	    param = termStackPop(stack);
	    destination->data.type = BOOL;
	    destination->data.data.boolean = boolval(&param);
	    return error;
	    
      }else if(strcmp(item->paramF.string, "doubleval") == 0){	//doubleval(term)
	    param = termStackPop(stack);
	    destination->data.type = REAL;
	    destination->data.data.real = doubleval(&param);
	    return error;
	    
      }else if(strcmp(item->paramF.string, "intval") == 0){	//intval(term)
	    param = termStackPop(stack);
	    destination->data.type = INT;
	    destination->data.data.integer = intval(&param);
	    return error;
	  
      }else if(strcmp(item->paramF.string, "strval") == 0){	//strvalval(term)
	    param = termStackPop(stack);
	    destination->data.type = STRING;
	    destination->data.data.string = strval(&param);
	    return error;
	  
      }else if(strcmp(item->paramF.string, "get_string") == 0){	//get_string()
	    destination->data.type = STRING;
	    destination->data.data.string = get_string(&error);
	    return error;
	    
      }else if(strcmp(item->paramF.string, "put_string") == 0){	//put_string(string, string, ...)
	    int i = 0;
	    param = termStackPop(stack);
	    while(param.type != NIL){
		put_string(strval(&(param)), NULL);
		param = termStackPop(stack);
		i++;
	    }
	    destination->data.type = INT;
	    destination->data.data.integer = i;
            return error;
	    
      }else if(strcmp(item->paramF.string, "strlen") == 0){  	//strlen(string)
	    param = termStackPop(stack);
	    destination->data.type = INT;
	    destination->data.data.integer = stringStrlen(strval(&(param)));
	    return error;
	  
      }else if(strcmp(item->paramF.string, "get_substring") == 0){	//get_substring(string, integer, integer)
	    param = termStackPop(stack);
	    param2 = termStackPop(stack);
	    param3 = termStackPop(stack);
	    destination->data.type = STRING;
            destination->data.data.string = stringInit("b");
	    error = get_substring(strval(&param), &(destination->data.data.string), intval(&param2), intval(&param3));
	    return error;
	    
      }else if(strcmp(item->paramF.string, "find_string") == 0){	//find_string(string, string)
	    param = termStackPop(stack);
	    param2 = termStackPop(stack);
	    destination->data.type = INT;
	    destination->data.data.integer = find_string(strval(&param), strval(&param2));	
	    return error;
	    
      }else if(strcmp(item->paramF.string, "sort_string") == 0){	//sort_string(string)
	    param = termStackPop(stack);
	    destination->data.type = STRING;
            param.data.string = strval(&param);
	    destination->data.data.string = sort_string(param.data.string);
	    return error;
      }
  
  
  
  return NOT_FOUND;
}