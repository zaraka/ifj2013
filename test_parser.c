/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdio.h>
#include <stdlib.h>

#include "controller.h"
#include "error_codes.h"

char* getErrorMessage(int msg) {
    switch (msg) {
        case 0:
            return "Everything is ok";
            break;
        case ERR_ARR:
            return "Error in argument";
            break;
        case ERR_DIVIDE_ZERO:
            return "Error divide zero";
            break;
        case ERR_FLOAT2INT:
            return "Error double to int conversion";
            break;
        case ERR_INTERNAL:
            return "Internal Error";
            break;
        case ERR_LEX:
            return "Error in lex";
            break;
        case ERR_SEMAN:
            return "Error in seman";
            break;
        case ERR_SYNTAX:
            return "Error in syntax";
            break;
        default:
            return "Unknown error";
            break;
    }
}

/*
 * 
 */
int main(void) {
    printf("-----------------------TEST START-----------------\n");

    int result = parse("tests/intval1.php");

    printf("Return code: %s\n", getErrorMessage(result));
    return (EXIT_SUCCESS);
}


