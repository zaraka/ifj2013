/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "ial.h"

int boyerMoore(SString source, SString look) {
    int l;
    int lenghtString = (strlen(source.string));
    int lenghtSearch = (strlen(look.string)) - 1;
    int i = lenghtSearch;
    int j = lenghtSearch;
    while (1) {
        if (lenghtString >= i) {
            if (look.string[i] == source.string[i]) {
                if ((l = checkStr(lenghtSearch, i, source, look)) >= 0) {
                    
                    break;
                } else {
                    for (; j >= 0; j--) {
                        look.string[j + 1] = look.string[j];
                    }
                    look.string[j + 1] = ' ';
                    i++;
                    j = i;
                }
            } else {
                for (; j >= 0; j--) {
                    look.string[j + 1] = look.string[j];
                }
                look.string[j + 1] = ' ';
                i++;
                j = i;
            }
        }
        else return -1;

    }
    return l;
}

int checkStr(int lenght, int actlenght, SString source, SString look) {
    int place = actlenght - lenght;
    for (; place <= actlenght; actlenght--) {
        if (look.string[actlenght] != source.string[actlenght]) {
            return -1;
        }
    }
    return actlenght + 1;
}

SString heapSort(SString string){
    string=makeHeap(string);
    int i=string.stringSize;
    while(string.stringSize>0){
        string=swap(string,0,string.stringSize-1);
        string.stringSize--;
        string=makeHeap(string);
    }
    string.stringSize=i;
    return string;
}

SString makeHeap(SString string) {
    for (int i = string.stringSize / 2 - 1; i >= 0; i--) {
        string = maxHeapify(string, i);
    }
    return string;
}

SString maxHeapify(SString string, int father) {
    int son = father * 2 + 1;
    while (son < string.stringSize) {        
        if (son + 1 < string.stringSize && string.string[son + 1] > string.string[son])
            son++;
        if (string.string[son] > string.string[father])
            string = swap(string, father, son);
        father = son;
        son = father * 2;
    }
    return string;
}

SString swap(SString string, int father, int son) {
    char ass = string.string[father];
    string.string[father] =string.string[son];
    string.string[son] = ass;
    return string;
}

unsigned int hashFunction(const char *str, unsigned htable_size) {
    unsigned int h = 0;
    unsigned char *p;
    for (p = (unsigned char*) str;
            *p != '\0'; p++) h = 31 * h + *p;
    return h % htable_size;
}

/**
 * will clear hash table
 */
void htableClear(SHashtTable *t) {
    if (t == NULL) {
        return;
    } else {
        SHTableItem *item = NULL, *next_item;

        for (unsigned i = 0; i < t->htable_size; i++) {
            item = t->ptr[i];

            /*uvolneni vsech polozek ze seznamu*/
            while (item != NULL) {
                next_item = item->next;
                free(item->key);
                free(item);
                item = next_item;
            }

            t->ptr[i] = NULL;
        }
    }
}

/**
 * apply function on each item
 */
void htableForeach(SHashtTable *t, void (*p_func)(SHTableItem *item)) {
    SHTableItem *item;

    for (unsigned i = 0; i < t->htable_size; i++) {
        item = t->ptr[i];

        while (item != NULL) {
            p_func(item);
            item = item->next;
        }
    }

}

/**
 * clear hashtable
 */
void htableFree(SHashtTable *t) {
    if (t == NULL) {
        return;
    } else {
        htableClear(t);
        free(t);
    }
}

/**
 * initialize hash table
 */
SHashtTable* htableInit(unsigned size) {
    SHashtTable *htable;

    htable = (SHashtTable *) malloc(sizeof (SHashtTable) + sizeof (SHTableItem *) * size);
    
    if (htable == NULL) {
        return NULL;
    }

    /*Ulozeni velikosti tabulky*/
    htable->htable_size = size;

    /*nastaveni vsech ukazatelu na NULL*/
    for (unsigned i = 0; i < size; i++) {
        htable->ptr[i] = NULL;
    }

    return htable;
}

SHTableItem * htableLookup(SHashtTable *t, const char *key) {
    unsigned position, key_size;
    SHTableItem *item, *last;
    
    position = hashFunction(key, t->htable_size);
    
    item = t->ptr[position];
    key_size = sizeof (char) * (strlen(key) + 1);

    /*hledani klice v tabulce*/
    while (item != NULL) {
        if (strcmp(item->key, key) == 0) {
            return item;
        }
        last = item;
        item = item->next;
    }

    item = (SHTableItem*) malloc(sizeof (SHTableItem));
    item->key = (char *) malloc(key_size);

    if (item == NULL) {
        return NULL;
    }

    /*vytvoreni noveho zaznamu*/
    //item->data;
    memcpy(item->key, key, key_size);
    item->next = NULL;

    if (t->ptr[position] == NULL) {
        t->ptr[position] = item;
    } else {
        last->next = item;
    }

    return item;
}

SHTableItem * htableFind(SHashtTable *t, const char *key) {
    unsigned position;
    SHTableItem *item;

    position = hashFunction(key, t->htable_size);

    item = t->ptr[position];
    
    /*hledani klice v tabulce*/
    while (item != NULL) {
        if (strcmp(item->key, key) == 0) {
            return item;
        }
        item = item->next;
    }

    return NULL;
}

void htableRemove(SHashtTable *t, const char *key) {
    SHTableItem *item, *last = NULL;
    unsigned position;

    position = hashFunction(key, t->htable_size);

    item = t->ptr[position];

    while (item != NULL) {
        if (strcmp(item->key, key) == 0) {
            if (last == NULL) {
                t->ptr[position] = item->next;
            } else {
                last->next = item->next;
            }
            free(item->key);
            free(item);
            break;
        }
        last = item;
        item = item->next;
    }

}

void htableStatistics(SHashtTable *t) {
    unsigned max = 0, min = 0, all = 0, counter = 0, first = 1;
    SHTableItem *item;


    for (unsigned i = 0; i < t->htable_size; i++) {
        item = t->ptr[i];
        counter = 0;
        while (item != NULL) {
            counter++;
            item = item->next;
        }
        if (first == 1) {
            first = 0;
            max = counter;
            min = counter;
        }
        all += counter;
        if (counter < min)
            min = counter;
        if (counter > max)
            max = counter;
    }

    printf("maximalni delka seznamu je: %u\n", max);
    printf("minimalni delka seznamu je: %u\n", min);
    printf("prumerna delka seznamu je: %f\n", (double) all / t->htable_size);
}

SHashtTable* htableCopy(SHashtTable *input){
    SHTableItem *item, *itemInput;
    
    SHashtTable *output;
    output = htableInit(input->htable_size);
    
    for(unsigned i = 0; i < input->htable_size; i++){
         
         itemInput = input->ptr[i];
         
         
         if(itemInput != NULL){
                item = (SHTableItem*) malloc(sizeof (SHTableItem));
                if(item == NULL)
                    return NULL;
                
                item->data.type = itemInput->data.type;
                if(item->data.type == STRING)
                    item->data.data.string = stringInit(itemInput->data.data.string.string);
                else
                    item->data.data = itemInput->data.data;
                
                item->key = (char*) malloc(sizeof(char) * (strlen(itemInput->key)+1) );
                if(item->key == NULL)
                    return NULL;
                strcpy(item->key, itemInput->key);
                
                
                itemInput = itemInput->next;

                output->ptr[i] = item;
                item = item->next;
         }
         
         
         while(itemInput != NULL){
                item = (SHTableItem*) malloc(sizeof (SHTableItem));
                if(item == NULL)
                    return NULL;
                item->data.type = itemInput->data.type;
                if(item->data.type == STRING)
                    item->data.data.string = stringInit(itemInput->data.data.string.string);
                else
                    item->data.data = itemInput->data.data;
                
                item->key = (char*) malloc(sizeof(char) * (strlen(itemInput->key)+1) );
                if(item->key == NULL)
                    return NULL;
                strcpy(item->key, itemInput->key);                
                
                itemInput = itemInput->next;
                item = item->next;
         }
        
        
        
    }
    
    return output;
}

SString sort_string(SString string){
    string=heapSort(string);
    return string;
}

int find_string(SString string, SString searchString){
    int result = boyerMoore(string,searchString);
    if(result == -1){
        return result;
    } else {
        //Oukej, ten člověk co psal ten algoritmus nahoře je idiot...
        return result + 1;
    }
}