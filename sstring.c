/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "sstring.h"
#include "error_codes.h"

#define SPACE 32
#define TILDE 126
#define ALLOCATED 10

int stringStrlen(SString string) {
    return string.stringSize;
}

/**
 * inicializace struktury SString
 * do char* string se uklada string
 * do int stringSize pak jeho delka
 * alokace probiha po 10b
 * @param str
 * @return SString
 */
SString stringInit(char* str) {
    SString string;

    string.stringSize = strlen(str);
    
    string.string = (char*) malloc(ALLOCATED * (string.stringSize / ALLOCATED + 1));

    if (string.string == NULL) {
        stringDestroy(&string);
        string.stringSize = -1; //this is like Error
        return string;
    }
    strcpy(string.string, str);
    if (string.string[string.stringSize] != '\0') {
#ifdef DEBUG_STRING
        fprintf(stderr, "Warning. string does not have end! corrected.\n");
#endif
        string.string[string.stringSize] = '\0';
        string.stringSize++;
    }
    return string;

}

void stringDestroy(SString *string) {
    if (string->string != NULL) {
        free(string->string);
        string->string = NULL;
        string->stringSize = 0;
    }
}

/**
 * pridani znaku na konec retezce
 * navysi se pocet znaku
 * na posledni index je pridan ukoncovaci znak
 * presahneli se naalokovana pamet, naalokuje se dalsich 10b
 * @param string
 * @param character
 * @return 
 */
int stringAddChar(SString *string, char character) {
    string->stringSize += 1;
    if ((string->stringSize % ALLOCATED) == 0) {
        string->string = (char*) realloc(string->string, sizeof (char)*(ALLOCATED * (string->stringSize / ALLOCATED + 1)));
    }

    if (string->string == NULL) {
        stringDestroy(string);
        return ERR_INTERNAL;
    }
    string->string[string->stringSize - 1] = character;
    string->string[string->stringSize] = '\0';
    return 0;
}

/**
 * odebrani posledniho znaku z retezce
 * snizi se delka retezce ulozena ve strukture
 * realokace se neprovadi 
 * @param string
 * @return 
 */
int stringRemoveChar(SString *string) {
    string->stringSize -= 1;
    string->string[string->stringSize] = '\0';
    return 0;
}

/**
 * nacita string ze standartniho vstupu
 * postupne se alokuje po 10b
 * po nacteni se nakonec retezce prida ukoncovaci znak
 * @param err_code
 * @return 
 */
SString get_string(int *err_code) {
    SString string;
    string.string = (char*) malloc(sizeof (char)*ALLOCATED);
    *err_code=0;
    if (string.string == NULL) {
        stringDestroy(&string);
        *err_code = ERR_INTERNAL;
        return string;
    }    
    int i = 0;
    while ((string.string[i] = getchar()) != '\n') {
        if (string.string[i] == EOF) {
            break;
        }
        i++;
        if ((i % ALLOCATED) == 0) {
            string.string = (char*) realloc(string.string, sizeof (char)*(i + ALLOCATED));
            if (string.string == NULL) {
                stringDestroy(&string);
                *err_code=ERR_INTERNAL;
                return string;
            }
        }
    }
    string.string[i] = '\0';
    string.stringSize = i;
    return string;
}

/**
 * vytvari podretezec
 * aby nedoslo k uvolneni puvodniho retezce,
 * je pouzity pomocny retezec a pouziva se predem naalokovany retezec,
 * ktery je uvolnen a znovu naalokovan
 * @param string
 * @param outString
 * @param start
 * @param stop
 * @return 
 */
int get_substring(SString string, SString *outString, int start, int stop) {
    if ((start < 0) || (stop < 0) || (start > stop) || (start >= string.stringSize) || (stop > string.stringSize)) {
        return ERR_SEMAN_OTHERS;
    }
    char *store = (char*) malloc(ALLOCATED * (string.stringSize / ALLOCATED + 1));
    strcpy(store, string.string);
    free(outString->string);
    outString->stringSize = stop - start + 1;
    outString->string = (char*) malloc(ALLOCATED * (outString->stringSize / ALLOCATED + 1));
    for (int i = 0; i < outString->stringSize; i++) {
        outString->string[i] = store[start + i];
    }
    
    outString->string[outString->stringSize] = '\0';
    
    free(store);
    return 0;
}

/**
 * funkce vypisuje za sebou retezce z parametru
 * jako posledni parametr se ocekava NULL, ktery ukoncuje cyklus vypisovani
 * @param string
 * @param ...
 * @return 
 */
int put_string(SString string, ...) {
    int paramCount = 0;
    SString oneString = string;
    va_list paramString;
    va_start(paramString, string);
    for (; oneString.string != NULL; paramCount++) {
        printf("%s", oneString.string);
        oneString = va_arg(paramString, SString);
    }
    va_end(paramString);
    return paramCount;
}

int compare_strings(SString fir,SString sec){
    return strcmp(fir.string,sec.string);
}

SString concate_string(SString fir,SString sec){
    int size = fir.stringSize + sec.stringSize;
    SString string;
    string.string = (char*) malloc(ALLOCATED * (size / ALLOCATED + 1));
    int i = 0;
    int lenF = stringStrlen(fir);
    int length = lenF+stringStrlen(sec);
    while(i<length){
        if(lenF>i){
            string.string[i]=fir.string[i];
        }else{
            string.string[i]=sec.string[i-lenF];
        }
        i++;
    }
    
    return string;
}