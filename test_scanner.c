/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdio.h>
#include <stdlib.h>

#include "scanner.h"


/*
 *
 */
int main(int argc, char** argv) {

    if(argc < 2){
        fprintf(stderr, "Not enough parameters");
        //writeUsage();
        return 99;
    }

    /**
    initScanner(argv[1]);

    SToken token;
    do {
        token = getToken();
        printToken(token);
    } while (token.type != LEX_ENDOF);

    destroyScanner();
    */

    SString hello_world = stringInit("Hello World!");
    //SString look = stringInit("I dont know!");
    get_substring(hello_world, &hello_world, 6, 10);
    put_string(hello_world, NULL);
    //printf("size %d '%s'\n", look.stringSize, look.string);
    //put_string(look, NULL);

    stringDestroy(&hello_world);
    //stringDestroy(&look);
    return (EXIT_SUCCESS);
}

