/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef INTERPRET_H
#define	INTERPRET_H

#include <stdio.h>
#include "ta_code.h"
#include "symbol_table.h"

#ifdef	__cplusplus
extern "C" {
#endif
	typedef struct STermStack{
	  STerm *data;
	  int size;
	}STermStack;
  
  
	int interStart(STacTable *tacTable, SMapTable * mapTable, SHashtTable *function, int *returnCode);
  
	int interRekRun(STacTable *tacTable, SMapTable * mapTable, SMapTable * originalTable, SHashtTable *function, int actualTable, STermStack *stack, int *returnCode);
	
	int interDoOperation(STacItem *item, SHashtTable *table); 

	STermStack* termStackInit();
	
	void termStackDestroy(STermStack *stack);
	
	STerm termStackPop(STermStack *stack);
	
	int termStackPush(STermStack *stack, STerm term);
	
	int checkInBuildFunction(STacItem *item, SHashtTable *table, STermStack *stack);
	
	
#ifdef	__cplusplus
}
#endif

#endif	/* INTERPRET_H */

 
