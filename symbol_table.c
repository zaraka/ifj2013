/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "symbol_table.h"
#include "error_codes.h"

#define TABALL 32

SMapTable* initSMap() {
    SMapTable* map = (SMapTable*) malloc(sizeof (SMapTable));
    if (map == NULL) {
        return NULL;
    }

    map->tables = (SHashtTable**) malloc(sizeof (SHashtTable*) * TABALL);
    if (map->tables == NULL) {
        free(map);
        return NULL;
    }
    map->size = 0;

    return map;
}

/**
 * insertSMap
 * @param map to insert a table
 * @return 0 if success ERR_INTERNAL if fail
 */
int insertSMap(SMapTable* map) {
    if (map == NULL) {
        return ERR_INTERNAL;
    } else {
        if (map->size % (TABALL) == 0) {
            map->tables = (SHashtTable**) realloc(map->tables, sizeof (SHashtTable*) * TABALL * (1 + (map->size / TABALL)));
            if (map->tables == NULL) {
                free(map->tables);
                return ERR_INTERNAL;
            }
        }

        SHashtTable* table = htableInit(HASH_TABLE_SIZE);
        if (table == NULL) {
            return ERR_INTERNAL;
        }
        map->tables[map->size] = table;
        map->size++;
        return 0;
    }
}

void destroySMap(SMapTable* map) {
    if (map == NULL) {
        return;
    }

    for (int i = 0; i < map->size; i++) {
        htableFree(map->tables[i]);
    }
    free(map->tables);
    free(map);
    map = NULL;
}

void printHTItem(SHTableItem *item) {
    if(item == NULL){
        return;
    }
    printf("%s\t", item->key);
    printTerm(&item->data);
    printf("\n");
}

void printSMap(SMapTable* map) {
    if (map == NULL) {
        return;
    }
    for (int i = 0; i < map->size; i++) {
        printf("Table [%d]--------------------------\n", i);
        htableForeach(map->tables[i], printHTItem);
        printf("-----------------------------------\n");
    }
}

STable* sTableInit() {
    STable *table;
    table = (STable*) malloc(sizeof (STable));
    if (table == NULL) {
        return NULL;
    }
    table->tables = (SHashtTable**) malloc(sizeof (SHashtTable*) * TABALL);
    if (table->tables == NULL) {
        free(table);
        return NULL;
    }
    table->top = -1;
    return table;
}

int sTablePush(STable *sTable) {
    sTable->top++;
    if (sTable->top % (TABALL) == 0) {
        sTable->tables = (SHashtTable**) realloc(sTable->tables, sizeof (SHashtTable*) * TABALL * (1 + (sTable->top / TABALL)));
        if (sTable->tables == NULL) {
            sTableFlush(sTable); //Everything is fucked
            return -1;
        }
    }

    //need to allocate another table
    sTable->tables[sTable->top] = htableInit(HASH_TABLE_SIZE);
    if (sTable->tables[sTable->top] == NULL) {
        return -1;
    }
    return 0;
}

SHashtTable* sTableTop(STable *sTable) {
    if (sTable->top == -1)
        return NULL;
    return (sTable->tables[sTable->top]);
}

void sTablePop(STable *sTable) {
    if (sTable->top == -1)
        return;
    free(sTable->tables[sTable->top]);
    sTable->top--;
}

void sTableFlush(STable *sTable) {
    free(sTable->tables);
    free(sTable);
    sTable = NULL;
}