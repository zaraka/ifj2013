/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */
#include "controller.h"
#include "error_codes.h"     
#include "ass_to_tac.h"
#include "interpret.h"

char* getErrType(int code) {
    switch (code) {
        case ERR_ARR:
            return "ERROR: Term has wrong type";
            break;
        case ERR_DIVIDE_ZERO:
            return "ERROR: Division by zero";
            break;
        case ERR_FLOAT2INT:
            return "ERROR: Cannot convert integer to double";
            break;
        case ERR_INTERNAL:
            return "ERROR: Internal error";
            break;
        /*case ERR_LEX:
            return "ERROR: Invalid token";
            break;*/
        case ERR_MISS_PARAM:
            return "ERROR: Missing param";
            break;
        case ERR_NOT_DECLARED:
            return "ERROR: Variable not declared";
            break;
        case ERR_SEMAN:
            return "ERRPR: Function not declared or redeclared";
            break;
        case ERR_SEMAN_OTHERS:
            return "ERROR: Some other Semantic error";
            break;
        /*case ERR_SYNTAX:
            return "ERROR: Syntax Error";
            break;*/
        default: 
            return " ";
            break;
    }
}

void writeError(int code) {
    fprintf(stderr, "%s\n", getErrType(code));
}

int parse(const char *filename) {
#ifdef DEBUG_CONTROLLER
    printf("start parser %s %s %s\n", __DATE__, __TIME__, filename);
#endif


#ifdef DEBUG_CONTROLLER
    printf("initialize maps and trees\n");
#endif
    //Initialization
    //Controller should define Symbol table for functions and global symbol table
    if (initializeGlobals() == ERR_INTERNAL) {
        writeError(ERR_INTERNAL);
        return ERR_INTERNAL;
    }

    //Let us create global table!
    if (insertSMap(_map) == ERR_INTERNAL) {
        freeGlobals();
        writeError(ERR_INTERNAL);
        return ERR_INTERNAL;
    }

    SASS *tree;
    tree = (SASS*) malloc(sizeof (SASS));
    if (tree == NULL) {
        freeGlobals();
        writeError(ERR_INTERNAL);
        return ERR_INTERNAL;
    }
    tree->root = NULL;

    int result = getASS(filename, tree);
#ifdef DEBUG_CONTROLLER
    printASS(tree->root);
    printSMap(_map);
#endif
    if (result != 0) {
        writeError(result);
        freeGlobals();
        destroyTree(tree->root);
        free(tree);
        return result;
    }

    STacTable table = STacTableInit();
    result = convASS2TAC(&table, _map, _functionsTable, tree->root);
#ifdef DEBUG_CONTROLLER
    STacPrintTable(&table);
#endif
    if (result != 0) {
        writeError(result);
        freeGlobals();
        destroyTree(tree->root);
        free(tree);
        STacTableDestroy(&table);
        return result;
    }
#ifdef DEBUG_CONTROLLER
#ifdef UNIX_COLORS
    printf("\033[31m");
#endif
    printf("--------------INTERPRET START-------------\n");
#ifdef UNIX_COLORS
    printf("\033[39m");
#endif
#endif    
    int returnCode; // Tohle nás nakonec nezajímá
    result = interStart(&table, _map, _functionsTable, &returnCode);
    if (result != 0) {
        writeError(result);       
    }
#ifdef DEBUG_CONTROLLER
#ifdef UNIX_COLORS
    printf("\033[31m");
#endif
    printf("\n--------------INTERPRET END-------------\n");
#ifdef UNIX_COLORS
    printf("\033[39m");
#endif
#endif      
    STacTableDestroy(&table);
    freeGlobals();
    destroyTree(tree->root);
    free(tree);
    return result;
}