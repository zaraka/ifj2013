/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef CONTROLLER_H
#define	CONTROLLER_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "parser.h"  

    /**
     * Main controller function
     * @param filename to parse
     * @return 0 in case of success, ERR_CODES otherwise
     */
    int parse(const char *filename);

#ifdef	__cplusplus
}
#endif

#endif	/* CONTROLLER_H */

