/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "controller.h" 
#include "scanner.h"

void writeUsage(){
    fprintf(stderr, "USAGE:\nifj <filename.php>\n");
}

/*
 * 
 */
int main(int argc, char** argv) {
    if(argc < 2){
        fprintf(stderr, "Not enough parameters\n");
        writeUsage();
        return 99;
    }
    
    //call controller to do all magic
    int result = parse(argv[1]);    
    return (result);
    
}
