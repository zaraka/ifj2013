/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */
#ifndef ASSTOTAC_H
#define	ASSTOTAC_H

#include "parser.h"
#include "ta_code.h"

#ifdef	__cplusplus
extern "C" {
#endif

  int convASS2TAC(STacTable *tacTable,struct SMapTable * mapTable, SHashtTable *function, SASSItem *tree);
  
  int convRekRun(STacTable *tacTable,struct SMapTable * mapTable, SHashtTable *function, SASSItem *tree, int actualTable);

  int convRekParam(STacTable *tacTable,struct SMapTable * mapTable, SHashtTable *function, SASSItem *tree, int actualTable);
  
  int convRekParamCall(STacTable *tacTable,struct SMapTable * mapTable, SHashtTable *function, SASSItem *tree, int actualTable);


#ifdef	__cplusplus
}
#endif

#endif	/* ASSTOTAC_H */

 
