/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */
#ifndef CONSTANTS_H
#define	CONSTANTS_H

#ifdef	__cplusplus
extern "C" {
#endif
    
    enum ERR_CODES {
        ERR_LEX = 1,
        ERR_SYNTAX = 2,
        ERR_SEMAN = 3,
        ERR_MISS_PARAM = 4,
        ERR_NOT_DECLARED = 5,
        ERR_DIVIDE_ZERO = 10,
        ERR_FLOAT2INT = 11,
        ERR_ARR = 12,
        ERR_SEMAN_OTHERS = 13,
        ERR_INTERNAL = 99
    };


#ifdef	__cplusplus
}
#endif

#endif	/* CONSTANTS_H */

