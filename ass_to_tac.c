/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */
#include "ass_to_tac.h"

#define NILL -1

int _label;

int convASS2TAC(STacTable *tacTable, SMapTable * mapTable, SHashtTable *function, SASSItem *tree) {
    int actualTable = 0;
    _label = 0;
    STacList list;
    SHTableItem *tableItem;
    char buffer[32];
    
    _localVariable++;
    sprintf(buffer, "%d", NILL);
    tableItem = htableLookup(mapTable->tables[actualTable], buffer);
    tableItem->data.type = NIL;
    
    for(int i = 0; i < mapTable->size; i++){
        list = STacListInit();
        STacListInsert(tacTable, list);
    }
    
    convRekRun(tacTable, mapTable, function, tree, actualTable );
    
    return 0;
}

int convRekRun(STacTable *tacTable, SMapTable * mapTable, SHashtTable *function, SASSItem *tree, int actualTable ) {
    char buffer[32];
    STacItem item;
    int lastLabel, gotoLabel;
    SHTableItem *tableItem;
    SString emptyString;
    emptyString.string = NULL;
    emptyString.stringSize = 0;
    
    

            
    item.label = -1;

    if (tree->token.type <= LEX_NOT_EQUAL) {
        if (tree->lItem->token.type != LEX_IDENT && tree->rItem->token.type != LEX_IDENT) {
            convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );
            sprintf(buffer, "%d", _localVariable);
            item.paramF = stringInit(buffer);
            
            convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );
             sprintf(buffer, "%d", _localVariable);
            item.paramS = stringInit(buffer);
            
            _localVariable++;
            sprintf(buffer, "%d", _localVariable);
            item.destination = stringInit(buffer);

        } else if (tree->rItem->token.type != LEX_IDENT) {
            convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );

            item.paramF = stringInit(tree->lItem->token.data.string);
            sprintf(buffer, "%d", _localVariable);
            item.paramS = stringInit(buffer);
            _localVariable++;
            sprintf(buffer, "%d", _localVariable);
            item.destination = stringInit(buffer);

        } else if (tree->lItem->token.type != LEX_IDENT) {
            convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );

            item.paramS = stringInit(tree->rItem->token.data.string);
            sprintf(buffer, "%d", _localVariable);
            item.paramF = stringInit(buffer);
            _localVariable++;
            sprintf(buffer, "%d", _localVariable);
            item.destination = stringInit(buffer);

        } else {
            item.paramF = stringInit(tree->lItem->token.data.string);
            item.paramS = stringInit(tree->rItem->token.data.string);
            _localVariable++;
            sprintf(buffer, "%d", _localVariable);
            item.destination = stringInit(buffer);

        }
        switch (tree->token.type) {
            case LEX_MUL:
                item.name = tac_mul;
                break;
            case LEX_DIV:
                item.name = tac_div;
                break;
            case LEX_PLUS:
                item.name = tac_add;
                break;
            case LEX_MINUS:
                item.name = tac_sub;
                break;
            case LEX_LESS_THAN:
                item.name = tac_ls;
                break;
            case LEX_MORE_THAN:
                item.name = tac_gr;
                break;
            case LEX_LESS_E_THAN:
                item.name = tac_lse;
                break;
            case LEX_MORE_E_THAN:
                item.name = tac_gre;
                break;
            case LEX_EQUAL:
                item.name = tac_eq;
                break;
            case LEX_NOT_EQUAL:
                item.name = tac_neq;
                break;

        }
        STacItemInsert(&(tacTable->list[actualTable]), item);

    } else if (tree->token.type == LEX_BEFORE_AFTER) {
        if(tree->lItem != NULL)
                convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );
        if(tree->rItem != NULL)
                convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );

    } else if (tree->token.type == LEX_IF) {
        if(tree->lItem->token.type != LEX_IDENT){
                convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );
        } else{
                item.paramF = stringInit(tree->lItem->token.data.string);
                item.name = tac_mov;
                sprintf(buffer, "%d", _localVariable);
                item.destination = stringInit(buffer);        
                item.paramS = emptyString;

        STacItemInsert(&(tacTable->list[actualTable]), item);
        }

        item.name = tac_if;
        item.destination = emptyString;
        sprintf(buffer, "%d", _localVariable);
        item.paramF = stringInit(buffer);
        sprintf(buffer, "%d", ++_label);
        item.paramS = stringInit(buffer);
        STacItemInsert(&(tacTable->list[actualTable]), item);
        _localVariable++;

        convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );

    } else if (tree->token.type == LEX_BIT_LOGIC) {
        lastLabel = _label;
        _label++;
        convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );
        gotoLabel = _label;
        _label++;
        
        item.label = -1;
        item.name = tac_goto;
        item.destination = emptyString;
        sprintf(buffer, "%d", gotoLabel);
        item.paramF = stringInit(buffer);
        item.paramS = emptyString;
        STacItemInsert(&(tacTable->list[actualTable]), item);
        
        //lastPos = tacTable->list[actualTable].size;
        item.label = lastLabel;
        item.name = tac_label;
        item.destination = emptyString;
        item.paramF = emptyString;
        item.paramS = emptyString;
        STacItemInsert(&(tacTable->list[actualTable]), item);
        item.label = -1;
        
        
        
        convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );
        
        item.label = gotoLabel;
        item.name = tac_label;
        item.destination = emptyString;
        item.paramF = emptyString;
        item.paramS = emptyString;
        STacItemInsert(&(tacTable->list[actualTable]), item);        
        item.label = -1;

        //tacTable->list[actualTable].item[lastPos + 1].label = lastLabel;

    } else if (tree->token.type == LEX_ASSIGN) {
        if (tree->rItem->token.type != LEX_IDENT) {
            convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );
            sprintf(buffer, "%d", _localVariable);
            
            item.paramF = stringInit(buffer);

        } else {
            item.paramF = stringInit(tree->rItem->token.data.string);
        }



        item.name = tac_mov;
        item.destination = stringInit(tree->lItem->token.data.string);
        item.paramS = emptyString;

        STacItemInsert(&(tacTable->list[actualTable]), item);
        _localVariable++;

    } else if (tree->token.type == LEX_WHILE) {
        gotoLabel = _label;
        _label++;
        
        item.label = gotoLabel;
        item.name = tac_label;
        item.destination = emptyString;
        item.paramF = emptyString;
        item.paramS = emptyString;
        STacItemInsert(&(tacTable->list[actualTable]), item);
        item.label = -1;

        if(tree->lItem->token.type != LEX_IDENT){
                convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );
        } else{
                item.paramF = stringInit(tree->lItem->token.data.string);
                item.name = tac_mov;
                sprintf(buffer, "%d", _localVariable);
                item.destination = stringInit(buffer);        
                item.paramS = emptyString;

        STacItemInsert(&(tacTable->list[actualTable]), item);
        }
        
        lastLabel = _label;
        _label++;
        item.name = tac_if;
        item.destination = emptyString;
        sprintf(buffer, "%d", _localVariable);
        item.paramF = stringInit(buffer);
        sprintf(buffer, "%d", lastLabel);
        item.paramS = stringInit(buffer);
        STacItemInsert(&(tacTable->list[actualTable]), item);
        _localVariable++;

        convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );
        
        item.label = -1;
        item.name = tac_goto;
        item.destination = emptyString;
        sprintf(buffer, "%d", gotoLabel);
        item.paramF = stringInit(buffer);
        item.paramS = emptyString;
        STacItemInsert(&(tacTable->list[actualTable]), item);
        
        item.label = lastLabel;
        item.name = tac_label;
        item.destination = emptyString;
        item.paramF = emptyString;
        item.paramS = emptyString;
        STacItemInsert(&(tacTable->list[actualTable]), item);        
        item.label = -1;
        //tacTable->list[actualTable].item[tacTable->list[actualTable].size - 1].label = lastLabel;
    
   
    } else if (tree->token.type == LEX_FUNCTION) {
        
        tableItem = htableLookup(function, tree->token.data.string);
        actualTable = tableItem->data.data.integer;
        
        convRekParam(tacTable, mapTable, function, tree->lItem, actualTable);
        
        if(tree->rItem != NULL)
                convRekRun(tacTable, mapTable, function, tree->rItem, actualTable );
        
        if(tacTable->list[actualTable].item[tacTable->list[actualTable].size - 1].name != tac_ret){
                item.paramF = emptyString;
                item.paramS = emptyString;
                item.destination = emptyString;
                item.name = tac_ret;
                
                STacItemInsert(&(tacTable->list[actualTable]), item);
        }
        
        actualTable = 0;
        
    } else if (tree->token.type == LEX_FUNCTION_CALL) {
        
        if(strcmp(tree->token.data.string, "put_string") == 0){
                sprintf(buffer, "%d", NILL);
                item.paramF = stringInit(buffer);
                item.paramS = emptyString;
                item.destination = emptyString;
                item.name = tac_push;
                
                STacItemInsert(&(tacTable->list[actualTable]), item);
        }
        
        convRekParamCall(tacTable, mapTable, function, tree->lItem, actualTable);
            
        sprintf(buffer, "%d", ++_localVariable);
        item.destination = stringInit(buffer);
        item.paramF = stringInit(tree->token.data.string);
        item.paramS = emptyString;
        item.name = tac_call;
        
        STacItemInsert(&(tacTable->list[actualTable]), item);
        //_ident++;
    
    } else if (tree->token.type == LEX_RETURN) {
        item.destination = emptyString;
        item.paramS = emptyString;
        item.name = tac_ret;
        
        if(tree->lItem == NULL){
            item.paramF = emptyString;
        }else if(tree->lItem->token.type == LEX_IDENT){
            item.paramF = stringInit(tree->lItem->token.data.string);
        }else{     
            convRekRun(tacTable, mapTable, function, tree->lItem, actualTable );
            sprintf(buffer, "%d", _localVariable);
            _localVariable++;
            item.paramF = stringInit(buffer);
        }
        
        STacItemInsert(&(tacTable->list[actualTable]), item);
        
        
    }



    return 0;
}

int convRekParam(STacTable *tacTable, SMapTable * mapTable, SHashtTable *function, SASSItem *tree, int actualTable){
    STacItem item;
    item.label = -1;
    
    SString emptyString;
    emptyString.string = NULL;
    emptyString.stringSize = 0;
    
    
    if(tree->rItem == NULL)
        return 0;
    
    convRekParam(tacTable, mapTable, function, tree->rItem, actualTable);
    
    item.destination = stringInit(tree->lItem->token.data.string);
    item.paramF = emptyString;
    item.paramS = emptyString;
    item.name = tac_pop;
    STacItemInsert(&(tacTable->list[actualTable]), item);
    
    return 0;
}

int convRekParamCall(STacTable *tacTable, SMapTable * mapTable, SHashtTable *function, SASSItem *tree, int actualTable){
    SString emptyString;
    emptyString.string = NULL;
    emptyString.stringSize = 0;
    
    STacItem item;
    item.label = -1;
    char buffer[32];
    
    if(tree->rItem == NULL)
        return 0;
    
    convRekParamCall(tacTable, mapTable, function, tree->rItem, actualTable);

    if(tree->lItem->token.type == LEX_IDENT)
        item.paramF = stringInit(tree->lItem->token.data.string);
    else{
        convRekRun(tacTable, mapTable, function, tree->lItem, actualTable);
        sprintf(buffer, "%d", _localVariable);
        item.paramF = stringInit(buffer);       
    }
    item.destination = emptyString;
    item.paramS = emptyString;
    item.name = tac_push;
    STacItemInsert(&(tacTable->list[actualTable]), item);
    
    return 0;
}