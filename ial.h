/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef IAL_H
#define	IAL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "term.h"

#ifdef	__cplusplus
extern "C" {
#endif

    SString makeHeap(SString string);

    SString maxHeapify(SString string, int father);

    SString swap(SString string, int father, int son);

    SString heapSort(SString string);

    int checkStr(int lenght, int actlenght, SString source, SString look);

    int boyerMoore(SString source, SString look);
    
    int find_string(SString source, SString look);
    
    SString sort_string(SString string);
    
    
    #define HASH_TABLE_SIZE 256

    //struktura pro polozku v seznamu
    typedef struct SHTableItem SHTableItem;
    
    struct SHTableItem {
        char *key;
        STerm data;
        //data will be Term
        struct SHTableItem *next;
    };


    //struktura pro seznam
    typedef struct SHashtTable SHashtTable;
    
    struct SHashtTable {
        unsigned htable_size;
        SHTableItem * ptr[];
    };

    unsigned int hashFunction(const char *str, unsigned htable_size);
    SHashtTable* htableInit(unsigned size);
    void htableClear(SHashtTable *t);
    void htableFree(SHashtTable *t);
    SHTableItem* htableLookup(SHashtTable *t, const char *key);
    SHTableItem* htableFind(SHashtTable *t, const char *key);
    void htableForeach(SHashtTable *t, void (*p_func)(SHTableItem *item));
    void htableRemove(SHashtTable *t, const char *key);
    void htableStatistics(SHashtTable *t);
    SHashtTable* htableCopy(SHashtTable *input);

#ifdef	__cplusplus
}
#endif

#endif	/* IAL_H */