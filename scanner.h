/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef SCANNER_H
#define	SCANNER_H

#include "globals.h"
#include "sstring.h"
#include <stdbool.h>

#ifdef	__cplusplus
extern "C" {
#endif

    /**
     * Stores all lexem types
     * Kdo to prepise bez meho vedomi bude muset si dat bodyshot z Ondreje Benuse a musi koupit tequilu
     */
    typedef enum TOKEN_TYPE {
        /**
         * * Multiplication operator
         * <blockquote>
         * $foo = 10 <b>*</b> $bar;
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_MUL = 0,
        /**
         * / Divide operator
         * <blockquote>
         * $foo = 10 <b>/</b> $bar;
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_DIV,
        /**
         * + Plus operator
         * <blockquote>
         * $foo = 10 <b>+</b> $bar;
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_PLUS,
        /**
         * - Minus operator
         * <blockquote>
         * $foo = 10 <b>-</b> $bar;
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_MINUS,
        /**
         * / Divide operator
         * <blockquote>
         * $foo = "abc" <b>.</b> $bar;
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_DOT,
        /**
         * less than expression
         * <blockquote>
         * if($foo <b>< </b> 10)
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_LESS_THAN,
        /**
         * more than expression
         * <blockquote>
         * if($foo <b>> </b> 10)
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_MORE_THAN,
        /**
         * less equals than expression
         * <blockquote>
         * if($foo <b><=</b> 10)
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_LESS_E_THAN,
        /**
         * more equals than expression
         * <blockquote>
         * if($foo <b>>=</b> 10)
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_MORE_E_THAN,
        /**
         * == Equals expression
         * <blockquote>
         * if($foo <b>===</b> 10)
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_EQUAL,
        /**
         * != Not equals expression
         * <blockquote>
         * if($foo <b>!==</b> 10)
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_NOT_EQUAL,
        /**
         * ( rounded left bracket
         * @parser
         * <b>TRUE</b>
         */
        LEX_BRACKET_R_L,
        /**
         * ) rounded right bracket
         * @parser
         * <b>TRUE</b>
         */
        LEX_BRACKET_R_R,
        /**
         * Identifier
         * <blockquote>
         * <b>$foo</b> = "hello world";
         * </blockquote>
         */
        LEX_IDENT,
        /**
         * ; Semicolon
         * <blockquote>
         * $foo = 10<b>;</b>
         * </blockquote>
         */
        LEX_SEMICOLON,
        /**
         * , Comma operator
         * <b>ONLY IN EXPRESSIONS</b>
         * <blockquote>
         * for($foo = 0<b>,</b>$bar = 10; $foo < 50; $foo++<b>,</b> $bar++){
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_COMMA,
        LEX_START,
        /**
         * @parser
         * <b>FALSE</b>
         */
        LEX_WORD_START,
        LEX_WORD,
        /**
         * Keyword
         * if, switch, for, while
         */
        LEX_KEYWORD,
        /**
         * data type Integer
         * <blockquote>
         * $foo = <b>10</b>;
         * </blockquote>
         */
        LEX_INT,
        /**
         * data type Double
         * <blockquote>
         * $foo = <b>2.5</b>;
         * </blockquote>
         */
        LEX_DOUBLE,
        /**
         * data type NULL (null) CASE SENSITIVE
         * <blockquote>
         * $foo = <b>null</b>;
         * </blockquote>
         */
        LEX_NULL,
        /**
         * data type BOOL (true or false) CASE SENSITIVE
         * <blockquote>
         * $foo = <b>true</b>;
         * </blockquote>
         */
        LEX_BOOL,
        /**
         * data type String
         * <blockquote>
         * $foo = <b>"hello"</b>;
         * </blockquote>
         */
        LEX_STRING_START,
        LEX_STRING,
        /**
         * 
         */
        LEX_IDENT_START,
        /**
         * End of file
         */
        LEX_ENDOF,
        /**
         * = Assign operator
         * <blockquote>
         * $foo <b>=</b> 10;
         * </blockquote>
         */
        LEX_ASSIGN,
        /**
         * += Assign plus operator
         * <blockquote>
         * $foo <b>+=</b> 10;
         * </blockquote>
         */
        LEX_ASSIGN_PLUS,
        /**
         * -= Assign minus operator
         * <blockquote>
         * $foo <b>-=</b> 10;
         * </blockquote>
         */
        LEX_ASSIGN_MINUS,
        /**
         * *= Assign multiply operator
         * <blockquote>
         * $foo <b>*=</b> 10;
         * </blockquote>
         */
        LEX_ASSIGN_MUL,
        /**
         * /= Assign divide operator
         * <blockquote>
         * $foo <b>/=</b> 10;
         * </blockquote>
         */
        LEX_ASSIGN_DIV,
        /**
         * % Modulo operator
         * <blockquote>
         * $foo = 10 <b>%</b> $bar;
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_MODULO,
        /**
         * ! not operator
         * @parser
         * <b>TRUE</b>
         */
        LEX_NOT,
        /**
         * ++ Increment operator
         * <blockquote>
         * for($foo = 0,$bar = 10; $foo < 50; $foo<b>++</b>, $bar--){
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_INC,
        /**
         * -- Decrement operator
         * <blockquote>
         * for($foo = 0,$bar = 10; $foo < 50; $foo++, $bar<b>--</b>){
         * ...
         * </blockquote>
         * @parser
         * <b>TRUE</b>
         */
        LEX_DEC,
        /**
         * [ squared left bracket
         * @parser
         * <b>TRUE</b>
         */
        LEX_BRACKET_S_L,
        /**
         * ] squared right bracket
         * @parser
         * <b>TRUE</b>
         */
        LEX_BRACKET_S_R,
        /**
         * { complex left bracket
         * @parser
         * <b>TRUE</b>
         */
        LEX_BRACKET_C_L,
        /**
         * } complex right bracket
         * @parser
         * <b>TRUE</b>
         */
        LEX_BRACKET_C_R,
        /**
         * @parser
         * <b>FALSE</b>
         */
        LEX_QUESTION,
        /**
         * @parser
         * <b>FALSE</b>
         */
        LEX_PHP_START1,
        LEX_PHP_START2,
        LEX_PHP_START3,
        /**
         * <?php php start
         * @parser
         * <b>TRUE</b>
         */

        LEX_PHP_START,
        /**
         * ?> php end
         * @parser
         * <b>TRUE</b>
         */
        LEX_PHP_END,
        /**
         * Token doesn't make sense
         * @parser
         * <b>TRUE</b>
         */
        LEX_ERROR,
        /**
         * Internal error (e.g. malloc)
         * @parser
         * <b>TRUE</b>
         */
        LEX_ERROR_INTER,
        /**
         * @parser
         * <b>FALSE</b>
         */
        LEX_NEWLINE,
        LEX_BACKSLASH,
        LEX_COMMENT_SIMPLE,
        LEX_COMMENT_COMPLEX,
        LEX_COMMENT_COMPLEX_END,
        LEX_DOUBLE_POW,
        LEX_DOUBLE_POW_START,
        LEX_NOT_EQUAL_START,
        LEX_EQUAL_START,
        LEX_STRING_HEX_START,
        LEX_STRING_HEX,
        /**
         * @parser
         * <b>FALSE</b>
         */
        LEX_END,
        /**
         * These will be generated by parser
         */
        LEX_FUNCTION,
        LEX_IF,
        LEX_WHILE,
        LEX_BIT_LOGIC,
        LEX_FUNCTION_CALL,
        LEX_BEFORE_AFTER,
        LEX_RETURN,
        LEX_UNDEFINED,
        NON_TERM,
        LEX_OPENER
    } TOKEN_TYPE;

    typedef struct SToken {
        int type;
        SString data;
        int row;
        int column;
    } SToken;


    SToken tokenInit(int type, char* data, int row, int column);

    void tokenDestroy(SToken token);

    /**
     * Will return next token in a file
     * @return SToken
     */
    SToken getToken();

    /**
     *
     * will open file and setup data stream 
     * 
     * @param filename to open
     * @return true if success in opening file
     */
    bool initScanner(const char* filename);

    /**
     * will destroy scanner and clean up 
     */
    void destroyScanner();

    /**
     * will return a string representation of tokenType
     * @param type to recognize
     * @return string representation
     */
    char* getTokenType(int type);

    /**
     * This is somewhat buggy
     * @param token
     */
    void printToken(SToken token);

#ifdef	__cplusplus
}
#endif

#endif	/* SCANNER_H */

