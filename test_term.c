/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdio.h>
#include <stdlib.h>

#include "term.h"

int TEST = 1;

void printBool(int check){
    printf("c.%d. vystup boolu je: %s\n",TEST,check ? "true" : "false");
    TEST++;
}

void printInt(int check){
    printf("c.%d. vystup Intu je: %d\n",TEST,check);
    TEST++;
}

void printDouble(double check){
    printf("c.%d. vystup Double je: %f\n",TEST,check);
    TEST++;
}

void kontBool(STerm *term){
    int check;
    check = boolval(term);
    printBool(check);
}

void kontInt(STerm *term){
    int kon;
    kon = intval(term);
    printInt(kon);
}

void kontDouble(STerm *term){
    double dou;
    dou = doubleval(term);
    printDouble(dou);
}

int main(){
    
    STerm *term;
    SString str;
    term = (STerm *) malloc(sizeof(struct STerm));
    
    printf("TEST1 BOOL INT TRUE\n");
    term->type = 0;
    term->data.integer = 1;
    kontBool(term);
    printf("\n");
    
    printf("TEST2 BOOL INT FALSE\n");
    term->type=0;
    term->data.integer = 0;
    kontBool(term);
    printf("\n");
    
    printf("TEST3 BOOL NULL\n");
    term->type=3;
    kontBool(term);
    printf("\n");
    
    printf("TEST4 BOOL BOOLEAN TRUE\n");
    term->type=5;
    term->data.boolean = true;
    kontBool(term);
    printf("\n");
    
    printf("TEST5 BOOL BOOLEAN FALSE\n");
    term->type=5;
    term->data.boolean = false;
    kontBool(term);
    printf("\n");
    
    printf("TEST6 BOOL DOUBLE TRUE\n");
    term->type=1;
    term->data.real = 1.0;
    kontBool(term);
    printf("\n");
    
    printf("TEST7 BOOL DOUBLE FALSE\n");
    term->type=1;
    term->data.real = 0.0;
    kontBool(term);
     printf("\n");
     
    printf("TEST8 BOOL STRING FALSE\n");
    term->type=2;
    kontBool(term);
    printf("\n");
    
    printf("TEST9 BOOL STRING TRUE\n");
    term->type=2;
    str = stringInit("True");
    term->data.string.string = str.string;
    term->data.string.stringSize = str.stringSize;
    kontBool(term);
    printf("\n");
    
    printf("TEST10 INT Bool TRUE\n");
    term->type=5;
    term->data.integer = 1;
    kontInt(term);
    printf("\n");
    
    printf("TEST11 INT Bool FALSE\n");
    term->type=5;
    term->data.integer = 0;
    kontInt(term);
    printf("\n");
    
    printf("TEST12 INT double 1\n");
    term->type=1;
    term->data.real = 1.4343;
    kontInt(term);
    printf("\n");
    
    printf("TEST13 INT NULL 0\n");
    term->type=3;
    kontInt(term);
    printf("\n");
    
    printf("TEST14 INT string 121\n");
    term->type=2;
    str = stringInit("   121");
    term->data.string.string = str.string;
    term->data.string.stringSize = str.stringSize;
    kontInt(term);
      printf("\n");
      
    printf("TEST15 INT string 123456789\n");
    term->type=2;
    str = stringInit("1234A56789");
    term->data.string.string = str.string;
    term->data.string.stringSize = str.stringSize;
    kontInt(term);
    printf("\n");
    
    printf("TEST16 INT int 5\n");
    term->type=0;
    term->data.integer = 5;
    kontInt(term);
    printf("\n");
    
    printf("TEST17 DOUBLE bool FALSE\n");
    term->type=5;
    term->data.boolean = false;
    kontDouble(term);
    printf("\n");
    
    printf("TEST18 DOUBLE bool TRUE\n");
    term->type=5;
    term->data.boolean = true;
    kontDouble(term);
    printf("\n");
    
    printf("TEST19 DOUBLE null\n");
    term->type=3;
    kontDouble(term);
    printf("\n");
    
    printf("TEST20 DOUBLE int 32\n");
    term->type=0;
    term->data.integer = 32;
    kontDouble(term);
    printf("\n");
    
    printf("TEST21 DOUBLE real 23.456\n");
    term->type=1;
    term->data.real = 23.456;
    kontDouble(term);
    printf("\n");
    
    printf("TEST22 DOUBLE null 13\n");
    term->type=3;
    kontDouble(term);
    printf("\n");
    
    printf("TEST23 INT string 121\n");
    term->type=2;
    str = stringInit("   121");
    term->data.string.string = str.string;
    term->data.string.stringSize = str.stringSize;
    kontDouble(term);
    printf("\n");
    
    printf("TEST24 STRING booleam 1\n");
    term->type=5;
    term->data.boolean = true;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST25 STRING booleam 1\n");
    term->type=5;
    term->data.boolean = false;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST26 STRING real 2.234\n");
    term->type=1;
    term->data.real = 2.234;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST27 STRING real 2655.233344\n");
    term->type=1;
    term->data.real = 2655.233344;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST28 STRING int 12345\n");
    term->type=0;
    term->data.integer = 12345;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST29 STRING int 12345\n");
    term->type=0;
    term->data.integer = 12345;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST30 STRING string halooo\n");
    term->type=2;
    str = stringInit("halooo");
    term->data.string.string = str.string;
    term->data.string.stringSize = str.stringSize;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    printf("TEST31 STRING null 13\n");
    term->type=3;
    str = strval(term);
    printf("c.%d. vystup Intu je: %s\n",TEST,str.string);
    TEST++;
    printf("\n");
    
    stringDestroy(&str);
    destroyTerm(term);
     
}
