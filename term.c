/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "term.h"
#include "error_codes.h"
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

void destroyTerm(STerm *term) {
    //OMG kdo to psal????
    free(term);
}

bool boolval(STerm *term) {
    switch (term->type) {
        case INT:
            if (term->data.integer == 0) {
                return false;
            } else {
                return true;
            }
            break;

        case REAL:
            if (term->data.real == 0.0) {
                return false;
            } else {
                return true;
            }
            break;

        case STRING:
            if (term->data.string.stringSize > 0) {
                return true;
            } else {
                return false;
            }
            break;
        case BOOL:
            return term->data.boolean;
            break;
        case NIL:
            return false;
            break;
        default:
            return 13;
            break;
    }

}

double doubleval(STerm *term) {
    switch (term->type) {
        case BOOL:
            if (term->data.boolean) {
                return 1.0;
            } else {
                return 0.0;
            }
            break;
        case INT:
            return (double) term->data.integer;
            break;
        case STRING:
            if (stringStrlen(term->data.string) == 0) {
                return 0.0;
            } else {
                return strtod(term->data.string.string, NULL);
            }
            break;
        case REAL:
            return term->data.real;
            break;
        case NIL:
            return 0.0;
            break;
        default:
            return 13;
            break;
    }

}

int intval(STerm *term) {
    int result = 0;
    switch (term->type) {
        case BOOL:
            if (term->data.boolean) {
                return 1;
            } else {
                return 0;
            }
            break;
        case REAL:
            return (int) term->data.real;
            break;
        case STRING:
            if (stringStrlen(term->data.string) == 0) {
                return result;
            } else {
                return atoi(term->data.string.string);
            }
            break;
        case INT:
            return term->data.integer;
            break;
        case NIL:
            return 0;
            break;
        default:
            return 13;
            break;
    }
}

SString strval(STerm *term) {
    char temp[20];

    switch (term->type) {
        case BOOL:
            if (term->data.boolean) {
                return stringInit("1");
            } else {
                return stringInit("0");
            }
            break;
        case REAL:
            sprintf(temp, "%g", term->data.real);
            return stringInit(temp);
            break;
        case INT:
            sprintf(temp, "%d", term->data.integer);
            return stringInit(temp);
            break;
        case STRING:
            return term->data.string;
            break;
        case NIL:
            return stringInit(" ");
            break;
        default:
            return stringInit("13");
            break;
    }
}

char* getTermType(STerm* term){
    switch(term->type){
        case INT:
            return "Integer";
            break;
        case BOOL:
            return "Bool";
            break;
        case REAL:
            return "Real";
            break;
        case STRING:
            return "String";
            break;
        case NIL:
            return "Null";
            break;
        default:
            return "Unitiliazed";
            break;
    }
}

void printTerm(STerm* term){
    switch(term->type){
       case INT:
           printf("%s = %d\n", getTermType(term), term->data.integer);
            break;
        case BOOL:
            printf("%s = %d\n", getTermType(term), term->data.boolean);
            break;
        case REAL:
            printf("%s = %g\n", getTermType(term), term->data.real);
            break;
        case STRING:
            printf("%s = %s\n", getTermType(term), term->data.string.string);
            break;
        case NIL:
            printf("%s\n", getTermType(term));
            break;
        default:
            printf("Unknown\n");
            break;
    }
}