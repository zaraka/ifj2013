CC=gcc
CFLAGS=-std=c99 -Wall -Wextra -pedantic -g
BIN=ifj

.PHONY: clean

all: clean main.o  controller.o parser.o sstring.o scanner.o exten_stack.o ta_code.o symbol_table.o term.o globals.o ass_to_tac.o ial.o interpret.o
	$(CC) $(CFLAGS) main.o  controller.o parser.o sstring.o scanner.o exten_stack.o ta_code.o symbol_table.o term.o globals.o ass_to_tac.o ial.o interpret.o -o $(BIN)
	
test_scan: test_scanner.o sstring.o scanner.o
	$(CC) $(CFLAGS) test_scanner.o sstring.o scanner.o -o $(BIN)
	
test_stacks: clean test_stacks.o exten_stack.o sstring.o symbol_table.o
	$(CC) $(CFLAGS) test_stacks.o exten_stack.o sstring.o symbol_table.o -o $(BIN)
	
test_parse: clean test_parser.o controller.o parser.o sstring.o scanner.o exten_stack.o ta_code.o symbol_table.o term.o globals.o ass_to_tac.o ial.o interpret.o
	$(CC) $(CFLAGS) test_parser.o controller.o parser.o sstring.o scanner.o exten_stack.o ta_code.o symbol_table.o term.o globals.o ass_to_tac.o ial.o interpret.o -o $(BIN)

test_term: test_term.o sstring.o term.o
	$(CC) $(CFLAGS) test_term.o sstring.o term.o -o $(BIN)
	
test_hashtable: test_hashTable.o hash_table.o term.o sstring.o scanner.o parser.o exten_stack.o globals.o symbol_table.o
	$(CC) $(CFLAGS) test_hashTable.o hash_table.o term.o sstring.o scanner.o parser.o exten_stack.o globals.o symbol_table.o -o $(BIN)
	
main.o:
	$(CC) $(CFLAGS) -c main.c
controller.o:
	$(CC) $(CFLAGS) -c controller.c
scanner.o:
	$(CC) $(CFLAGS) -c scanner.c
parser.o:
	$(CC) $(CFLAGS) -c parser.c
ial.o:
	$(CC) $(CFLAGS) -c ial.c
sstring.o:
	$(CC) $(CFLAGS) -c sstring.c
term.o:
	$(CC) $(CFLAGS) -c term.c
test_scanner.o:
	$(CC) $(CFLAGS) -c test_scanner.c
exten_stack.o:
	$(CC) $(CFLAGS) -c exten_stack.c
ta_code.o:
	$(CC) $(CFLAGS) -c ta_code.c
symbol_table.o:
	$(CC) $(CFLAGS) -c symbol_table.c
test_stacks.o:
	$(CC) $(CFLAGS) -c test_stacks.c
interpret.o:	
	$(CC) $(CFLAGS) -c interpret.c
test_parser.o:
	$(CC) $(CFLAGS) -c test_parser.c
globals.o:
	$(CC) $(CFLAGS) -c globals.c
ass_to_tac.o:
	$(CC) $(CFLAGS) -c ass_to_tac.c	
test_hashTable.o:
	$(CC) $(CFLAGS) -c test_hashTable.c
	
clean:
	rm -f *.o $(BIN)