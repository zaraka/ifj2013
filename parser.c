/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "parser.h"

#include "error_codes.h"
#include "symbol_table.h"
#include "term.h"

char preced_table[16][16] = {
    {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '>', '>', '>', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '>', '>', '>', '>', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '<', '<', '<', '<', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '<', '<', '<', '<', '>', '>', '<', '>', '<', '>', '>'},
    {'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '=', '<', 'n', 'n'},
    {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'n', '>', 'n', '>', '>'},
    {'>', '>', '>', '>', '>', '>', '>', '>', '>', '>', '>', 'n', '>', 'n', '>', '>'},
    {'<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', '<', 'n', '<', 'n', 'n'},
    {'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n', 'n'}
};

//---------------------LL RULES PROTOTYPES HERE----------------------------
/**
 * 
 * @param token Input token
 * @param tree Pointer to a Pointer to ASS tree to store
 * @return ERR_CODE
 */
int ruleStart(SToken token, SASSItem **tree);
int ruleStList(SToken token, SASSItem **tree);
int ruleStat(SToken token, SASSItem **tree, int table);
int ruleIfList(SToken token, SASSItem **tree, int table);
int ruleFuncList(SToken token, SASSItem **tree, int table);
int ruleIdList(SToken token, SASSItem **tree, int table);
int ruleAssignList(SToken token, SASSItem **tree, int table);
int ruleIdidList(SToken token, SASSItem **tree, int table);
int ruleItemList(SToken token, SASSItem **tree, int table);
int ruleFirstId(SToken token, SASSItem **tree, int table);

void printTree(SASSItem *TempTree, char* sufix, char fromdir) {
    if (TempTree != NULL) {
        char* suf2 = (char*) malloc(strlen(sufix) + 4);
        strcpy(suf2, sufix);
        if (fromdir == 'L') {
            suf2 = strcat(suf2, "  |");
            printf("%s\n", suf2);
        } else
            suf2 = strcat(suf2, "   ");
        printTree(TempTree->rItem, suf2, 'R');
        if (TempTree->token.type == LEX_IDENT || TempTree->token.type == LEX_FUNCTION_CALL || TempTree->token.type == LEX_FUNCTION) {
            printf("%s  +-[%s|%s]\n", sufix, getTokenType(TempTree->token.type), TempTree->token.data.string);
        } else {
            printf("%s  +-[%s]\n", sufix, getTokenType(TempTree->token.type));
        }
        strcpy(suf2, sufix);
        if (fromdir == 'R')
            suf2 = strcat(suf2, "  |");
        else
            suf2 = strcat(suf2, "   ");
        printTree(TempTree->lItem, suf2, 'L');
        if (fromdir == 'R') printf("%s\n", suf2);
        free(suf2);
    }
}

void printASS(SASSItem *TempTree) {
    printf("=================================================\n");
    printf("Struktura binarniho stromu:\n");
    printf("\n");
    if (TempTree != NULL)
        printTree(TempTree, "", 'X');
    else
        printf("strom je prazdny\n");
    printf("\n");
    printf("=================================================\n");

}

void destroyASS(SASSItem* root) {
    if (root == NULL) {
        return;
    }
    if (root->lItem != NULL) {
        destroyASS(root->lItem);
    }
    if (root->rItem != NULL) {
        destroyASS(root->rItem);
    }
    tokenDestroy(root->token);
    free(root);
    root = NULL;
}

int getASS(const char* filename, SASS *tree) {

    //Setup Scanner
    if (!initScanner(filename)) {
        return ERR_INTERNAL;
    }

    SToken token = advGetToken(0, false);
    int result = ruleStart(token, &(tree->root));
    if (result != 0) {
        destroyASS(tree->root);
    }

    destroyScanner();
    return result;
}

/**
 * Test if token is start of Exppression
 * @param token
 * @param tree
 * @return TRUE if token is exprresion, FALSE otherwise
 */
int testExpr(SToken token) {
    if (token.type == LEX_IDENT || token.type == LEX_BRACKET_R_L) {
        return 1;
    } else {
        return 0;
    }
}

/**
 * Internal function for returning error code & message
 * @param token
 * @return ERR_CODES
 */
int writeSyntaxError(SToken token, int expectedToken) {
    if (token.type == LEX_ERROR) {
        return ERR_LEX;
    } else if(token.type == LEX_UNDEFINED){
        return ERR_NOT_DECLARED;
    } else {
        fprintf(stderr, "SYNTAX ERROR: Unexpected token %s\n", getTokenType(token.type));
        if (expectedToken != -1) {
            fprintf(stderr, "Expected token %s\n", getTokenType(token.type));
        }
        return ERR_SYNTAX;
    }
}

/**
 * Rule for nonterm START
 * @param token
 * @param tree
 * @return
 */
int ruleStart(SToken token, SASSItem **tree) {
#ifdef DEBUG_PARSER_RULES
    printf("rule START\n");
#endif
    if (token.type == LEX_PHP_START) {
        token.type = LEX_BEFORE_AFTER;
        (*tree) = createTree(token, NULL, NULL);
        token = advGetToken(0, false);
        return ruleStList(token, tree);
    } else {
        return writeSyntaxError(token, LEX_PHP_START);
    }
}

/**
 * Rule for nonterm ST-LIST
 * @param token
 * @param tree - this should be always BEFORE_AFTER tree
 * @return
 */
int ruleStList(SToken token, SASSItem **tree) {
    int result;
#ifdef DEBUG_PARSER_RULES
    printf("rule ST-LIST\n");
#endif
    if (token.type == LEX_ENDOF) {
        return 0;
    } else if (token.type == LEX_KEYWORD && (strcmp("function", token.data.string) == 0)) {
        //tokenDestroy(token);
        token = advGetToken(0, false);
        if (token.type != LEX_WORD) {
            return writeSyntaxError(token, LEX_WORD);
        }
        SHTableItem *function = htableFind(_functionsTable, token.data.string);
        if (function != NULL) {
            fprintf(stderr, "ERROR: Trying to redeclare function %s\n", token.data.string);
            return ERR_SEMAN;
        }
        // put this into functionsHashTable!
        function = htableLookup(_functionsTable, token.data.string);
        if (function == NULL) {
            return ERR_INTERNAL;
        }
        //insert function symbol table
        if (insertSMap(_map) == ERR_INTERNAL) {
            return ERR_INTERNAL;
        }
        int tableIndex = _map->size - 1;
        function->data.data.integer = tableIndex;
        function->data.type = INT;
        token.type = LEX_FUNCTION;
        (*tree)->lItem = createTree(token, NULL, NULL);
        if ((*tree)->lItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(_map->size - 1, false);
        if (token.type != LEX_BRACKET_R_L) {
            return writeSyntaxError(token, LEX_BRACKET_R_L);
        }
        tokenDestroy(token);
        token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->lItem->lItem = createTree(token, NULL, NULL);
        if ((*tree)->lItem->lItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(tableIndex, false);
        result = ruleFirstId(token, &(*tree)->lItem->lItem, tableIndex);
        if (result != 0) {
            return result;
        }
        token = advGetToken(tableIndex, false);
        if (token.type != LEX_BRACKET_C_L) {
            return writeSyntaxError(token, LEX_BRACKET_C_L);
        }
        tokenDestroy(token);
        token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->lItem->rItem = createTree(token, NULL, NULL);
        if ((*tree)->lItem->rItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(tableIndex, false);
        result = ruleIfList(token, &(*tree)->lItem->rItem, tableIndex);
        if (result != 0) {
            return result;
        }
        //tokenDestroy(token);
        token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem = createTree(token, NULL, NULL);
        if ((*tree)->rItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(0, false);
        return ruleStList(token, &(*tree)->rItem);
    } else if (token.type == LEX_IDENT || token.type == LEX_WORD || (token.type == LEX_KEYWORD && (strcmp(token.data.string, "return") == 0 || strcmp(token.data.string, "if") == 0 || strcmp(token.data.string, "while") == 0))) {
        result = ruleStat(token, &(*tree)->lItem, 0);
        if (result != 0) {
            return result;
        }
        token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem = createTree(token, NULL, NULL);
        if ((*tree)->rItem == NULL) {
            return ERR_INTERNAL;
        }
        SToken token = advGetToken(0, false);
        return ruleStList(token, &(*tree)->rItem);
    } else if (token.type == LEX_SEMICOLON) {
        token = advGetToken(0, false);
        return ruleStList(token, tree);
    } else if (token.type == LEX_PHP_END) {
        return 0;
    } else {
        return writeSyntaxError(token, -1);
    }
}

int ruleFirstId(SToken token, SASSItem **tree, int table) {
#ifdef DEBUG_PARSER_RULES
    printf("rule FIRST-ID\n");
#endif
    if (token.type == LEX_BRACKET_R_R) {
        return 0;
    } else if (token.type == LEX_IDENT) {
        (*tree)->lItem = createLeaf(token);
        if ((*tree)->lItem == NULL) {
            return ERR_INTERNAL;
        }
        token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem = createTree(token, NULL, NULL);
        if ((*tree)->rItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        return ruleIdidList(token, &(*tree)->rItem, table);
    } else {
        return writeSyntaxError(token, -1);
    }
}

/**
 * Rule for nonterm STAT
 * @param token
 * @param tree
 * @return
 */
int ruleStat(SToken token, SASSItem **tree, int table) {
    int result;
#ifdef DEBUG_PARSER_RULES
    printf("rule STAT %s\n", getTokenType(token.type));
#endif
    if (token.type == LEX_KEYWORD && strcmp(token.data.string, "if") == 0) {
        tokenDestroy(token);
        token.type = LEX_IF;
        (*tree) = createTree(token, NULL, NULL);
        token = advGetToken(table, false);
        if (token.type != LEX_BRACKET_R_L) {
            return writeSyntaxError(token, LEX_BRACKET_R_L);
        }
        tokenDestroy(token);
        token = advGetToken(table, true);
        if (!testExpr(token)) {
            return writeSyntaxError(token, -1);
        }
        result = precAnalytics(&token, &((*tree)->lItem), table, true);
        if (result != 0) {
            return result;
        }
        if (token.type != LEX_BRACKET_R_R) {
            return writeSyntaxError(token, LEX_BRACKET_R_R);
        }
        tokenDestroy(token);
        token = advGetToken(table, false);
        if (token.type != LEX_BRACKET_C_L) {
            return writeSyntaxError(token, LEX_BRACKET_C_L);
        }
        tokenDestroy(token);
        token = advGetToken(table, false);
        SToken treeToken = tokenInit(LEX_BIT_LOGIC, NULL, 0, 0);
        (*tree)->rItem = createTree(treeToken, NULL, NULL);
        treeToken = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem->lItem = createTree(treeToken, NULL, NULL);
        result = ruleIfList(token, &(*tree)->rItem->lItem, table);
        if (result != 0) {
            return result;
        }
        //tokenDestroy(token);
        token = advGetToken(table, false);
        if (token.type == LEX_KEYWORD && strcmp(token.data.string, "else") != 0) {
            return writeSyntaxError(token, -1);
        }
        tokenDestroy(token);
        token = advGetToken(table, false);
        if (token.type != LEX_BRACKET_C_L) {
            return writeSyntaxError(token, LEX_BRACKET_C_L);
        }
        tokenDestroy(token);
        treeToken = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem->rItem = createTree(treeToken, NULL, NULL);
        if ((*tree)->rItem->rItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        return ruleIfList(token, &(*tree)->rItem->rItem, table);
    } else if (token.type == LEX_KEYWORD && strcmp(token.data.string, "return") == 0) {
        //tokenDestroy(token);
        token.type = LEX_RETURN;
        (*tree) = createTree(token, NULL, NULL);
        if ((*tree) == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, true);
        if (!testExpr(token)) {
            return writeSyntaxError(token, -1);
        }
        result = precAnalytics(&token, &((*tree)->lItem), table, false);
        if (result != 0) {
            return result;
        }
        if (token.type == LEX_SEMICOLON) {
            return 0;
        } else {
            return writeSyntaxError(token, LEX_SEMICOLON);
        }
    } else if (token.type == LEX_KEYWORD && strcmp(token.data.string, "while") == 0) {
        tokenDestroy(token);
        token.type = LEX_WHILE;
        (*tree) = createTree(token, NULL, NULL);
        if ((*tree) == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        if (token.type != LEX_BRACKET_R_L) {
            return writeSyntaxError(token, LEX_BRACKET_R_L);
        }
        if (!testExpr(token)) {
            return writeSyntaxError(token, -1);
        }
        result = precAnalytics(&token, &(*tree)->lItem, table, true);
        if (result != 0) {
            return result;
        }
        if (token.type != LEX_BRACKET_R_R) {
            return writeSyntaxError(token, LEX_BRACKET_R_R);
        }
        tokenDestroy(token);
        token = advGetToken(table, false);

        if (token.type != LEX_BRACKET_C_L) {
            return writeSyntaxError(token, LEX_BRACKET_C_L);
        }
        tokenDestroy(token);
        SToken treeToken = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem = createTree(treeToken, NULL, NULL);
        if ((*tree)->rItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        return ruleIfList(token, &(*tree)->rItem, table);
    } else if (token.type == LEX_IDENT) {
        //this will be assign!
        SToken assign = tokenInit(LEX_ASSIGN, NULL, 0, 0);
        SASSItem* leaf = createLeaf(token);
        if (leaf == NULL) {
            return ERR_INTERNAL;
        }
        (*tree) = createTree(assign, leaf, NULL);
        if ((*tree) == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        if (token.type != LEX_ASSIGN) {
            return writeSyntaxError(token, LEX_ASSIGN);
        }
        tokenDestroy(token);
        token = advGetToken(table, true);
        return ruleAssignList(token, &(*tree)->rItem, table);
    } else if (token.type == LEX_SEMICOLON) {
        return 0;
    } else if (token.type == LEX_WORD) {
        token.type = LEX_FUNCTION_CALL;
        (*tree) = createTree(token, NULL, NULL);
        if ((*tree) == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        if (token.type != LEX_BRACKET_R_L) {
            return writeSyntaxError(token, LEX_BRACKET_R_L);
        }
        token.type = LEX_BEFORE_AFTER;
        (*tree)->lItem = createTree(token, NULL, NULL);
        if ((*tree)->lItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, true);
        return ruleItemList(token, &(*tree)->lItem, table);
    } else {
        return writeSyntaxError(token, -1);
    }
}

/**
 * Rule for nonterm IF-LIST
 * @param token
 * @param tree - this should be always BEFORE_AFTER
 * @return
 */
int ruleIfList(SToken token, SASSItem **tree, int table) {
    int result;
#ifdef DEBUG_PARSER_RULES
    printf("rule IF-LIST\n");
#endif
    if (token.type == LEX_BRACKET_C_R) {
        return 0;
    } else if (token.type == LEX_WORD || token.type == LEX_IDENT || (token.type == LEX_KEYWORD && (strcmp(token.data.string, "return") == 0 || strcmp(token.data.string, "if") == 0 || strcmp(token.data.string, "while") == 0 || strcmp(token.data.string, "return") == 0))) {
        result = ruleStat(token, &(*tree)->lItem, table);
        if (result != 0) {
            return result;
        }
        //tokenDestroy(token);
        token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem = createTree(token, NULL, NULL);
        if ((*tree)->rItem == NULL) {
            return ERR_INTERNAL;
        }
        token = advGetToken(table, false);
        return ruleIfList(token, &(*tree)->rItem, table);
    } else if (token.type == LEX_SEMICOLON) {
        token = advGetToken(table, false);
        return ruleIfList(token, tree, table);
    } else {
        return writeSyntaxError(token, -1);
    }
}

/**
 * Role for nonterm FUNC-LIST
 * @param token
 * @param tree
 * @return
 */
int ruleFuncList(SToken token, SASSItem **tree, int table) {
#ifdef DEBUG_PARSER_RULES
    printf("rule FUNC-LIST\n");
#endif
    if (token.type != LEX_WORD) {
        return writeSyntaxError(token, LEX_WORD);
    }
    token.type = LEX_FUNCTION_CALL;
    (*tree) = createTree(token, NULL, NULL);
    if ((*tree) == NULL) {
        return ERR_INTERNAL;
    }
    token = advGetToken(table, false);
    if (token.type != LEX_BRACKET_R_L) {
        return writeSyntaxError(token, LEX_BRACKET_R_L);
    }
    token.type = LEX_BEFORE_AFTER;
    (*tree)->lItem = createTree(token, NULL, NULL);
    if ((*tree)->lItem == NULL) {
        return ERR_INTERNAL;
    }
    token = advGetToken(table, true);
    return ruleItemList(token, &(*tree)->lItem, table);
}

/**
 * Rule for nonterm ID-LIST
 * @param token
 * @param tree - what the hell??
 * @return
 */
int ruleIdList(SToken token, SASSItem **tree, int table) {
#ifdef DEBUG_PARSER_RULES
    printf("rule ID-LIST\n");
#endif
    if (token.type != LEX_IDENT) {
        return writeSyntaxError(token, LEX_IDENT);
    }
    (*tree)->lItem = createLeaf(token);
    if ((*tree)->lItem == NULL) {
        return ERR_INTERNAL;
    }
    token = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
    (*tree)->rItem = createTree(token, NULL, NULL);
    if ((*tree)->rItem == NULL) {
        return ERR_INTERNAL;
    }
    token = advGetToken(table, false);
    return ruleIdidList(token, &(*tree)->rItem, table);
}

/**
 * Rule for nonterm ASSIGN-LIST
 * @param token
 * @param tree
 * @return
 */
int ruleAssignList(SToken token, SASSItem **tree, int table) {
    int result;
#ifdef DEBUG_PARSER_RULES
    printf("rule ASSIGN LIST\n");
#endif
    if (testExpr(token)) {
        result = precAnalytics(&token, tree, table, false);
        if (result != 0) {
            return result;
        }
        if (token.type == LEX_SEMICOLON) {
            return 0;
        } else {
            return writeSyntaxError(token, LEX_SEMICOLON);
        }
    } else if (token.type == LEX_WORD) {
        return ruleFuncList(token, tree, table);
    } else {
        return writeSyntaxError(token, -1);
    }
}

/**
 * Rule for nonterm IDID-List
 * @param token
 * @param tree
 * @return
 */
int ruleIdidList(SToken token, SASSItem **tree, int table) {
#ifdef DEBUG_PARSER_RULES
    printf("rule IDID-LIST\n");
#endif
    if (token.type == LEX_COMMA) {
        SToken token = advGetToken(table, false);
        return ruleIdList(token, tree, table);
    } else if (token.type == LEX_BRACKET_R_R) {
        return 0;
    } else {
        return writeSyntaxError(token, -1);
    }
}

/**
 * Rule for nonterm ITEM-LIST
 * @param token
 * @param tree - should accept BEFORE AFTER
 * @return
 */
int ruleItemList(SToken token, SASSItem **tree, int table) {
    int result;
#ifdef DEBUG_PARSER_RULES
    printf("rule ITEM-LIST\n");
#endif
    if (testExpr(token)) {
        result = precAnalytics(&token, &((*tree)->lItem), table, true);
        if (result != 0) {
            return result;
        }
        SToken treeToken = tokenInit(LEX_BEFORE_AFTER, NULL, 0, 0);
        (*tree)->rItem = createTree(treeToken, NULL, NULL);
        if ((*tree)->rItem == NULL) {
            return ERR_INTERNAL;
        }
        return ruleItemList(token, &(*tree)->rItem, table);
    } else if (token.type == LEX_COMMA) {
        token = advGetToken(table, true);
        return ruleItemList(token, tree, table);
    } else if (token.type == LEX_BRACKET_R_R) {
        return 0;
    } else {
        return writeSyntaxError(token, -1);
    }
}

SASSItem * createLeaf(SToken token) {
#ifdef DEBUG_TREE
#ifdef UNIX_COLORS
    printf("\033[33m");
#endif
    printf("Creating leaf %s\n", getTokenType(token.type));
#ifdef UNIX_COLORS
    printf("\033[39m");
#endif
#endif
    SASSItem* leaf = (SASSItem*) malloc(sizeof (SASSItem));
    if (leaf == NULL) {
        return NULL;
    } else {
        leaf->token = token;
        leaf->lItem = NULL;
        leaf->rItem = NULL;
        leaf->type = LEAF;
        return leaf;
    }
}

SASSItem * createTree(SToken token, SASSItem* left, SASSItem * right) {
#ifdef DEBUG_TREE
#ifdef UNIX_COLORS
    printf("\033[33m");
#endif
    printf("Creating tree %s\n", getTokenType(token.type));
#ifdef UNIX_COLORS
    printf("\033[39m");
#endif
#endif
    SASSItem* tree = (SASSItem*) malloc(sizeof (SASSItem));
    if (tree == NULL) {
        return NULL;
    } else {
        tree->token.type = token.type;
        tree->token.data.string = token.data.string;
        tree->token.data.stringSize = token.data.stringSize;
        tree->lItem = left;
        tree->rItem = right;
        tree->type = TREE;
        return tree;
    }
}

/**
 * Destroy ASS Item
 * @param item
 */
void destroySASSItem(SASSItem * item) {
    if (item != NULL) {
        tokenDestroy(item->token);
        free(item);
        item = NULL;
    }
}

void destroyTree(SASSItem* root) {
    if (root != NULL) {
        destroyTree(root->lItem);
        destroyTree(root->rItem);
        destroySASSItem(root);
    }
}

SToken advGetToken(int table, bool isExpr) {
    SToken token;
    do {
        token = getToken();
    } while (token.type == LEX_COMMENT_SIMPLE || token.type == LEX_COMMENT_COMPLEX);
#ifdef DEBUG_TOKENS
#ifdef UNIX_COLORS
    printf("\033[36m");
#endif
    if (token.data.string == NULL) {
        printf("TOKEN TYPE %s\n", getTokenType(token.type));
    } else {
        printf("TOKEN TYPE %s %s\n", getTokenType(token.type), token.data.string);
    }
#ifdef UNIX_COLORS
    printf("\033[39m");
#endif
#endif    

    if (token.type == LEX_IDENT) {
        //Handle Idents!
        SHTableItem *item;
        if (isExpr) {
            item = htableFind(_map->tables[table], token.data.string);
            if (item == NULL) {
                token.type = LEX_UNDEFINED;
                fprintf(stderr, "Variable %s doesn't exists\n", token.data.string);
            }
        } else {
            item = htableLookup(_map->tables[table], token.data.string);
            item->data.type = EMPTY;
        }
    } else if (token.type == LEX_INT) {
        //convert int to string
        char name[10];
        sprintf(name, "%d", _localVariable);
        _localVariable++;
        SHTableItem *item = htableLookup(_map->tables[table], name);
        item->data.type = INT;
        item->data.data.integer = atoi(token.data.string);
        //convert token to IDENT and return it
        stringDestroy(&token.data);
        token.type = LEX_IDENT;
        token.data = stringInit(name);
    } else if (token.type == LEX_DOUBLE) {
        char name[10];
        sprintf(name, "%d", _localVariable);
        _localVariable++;
        SHTableItem *item = htableLookup(_map->tables[table], name);
        item->data.type = REAL;
        item->data.data.real = strtod(token.data.string, NULL);
        stringDestroy(&token.data);
        token.type = LEX_IDENT;
        token.data = stringInit(name);
    } else if (token.type == LEX_STRING) {
        char name[10];
        sprintf(name, "%d", _localVariable);
        _localVariable++;
        SHTableItem *item = htableLookup(_map->tables[table], name);
        item->data.type = STRING;
        item->data.data.string = stringInit(token.data.string);
        stringDestroy(&token.data);
        token.type = LEX_IDENT;
        token.data = stringInit(name);
    } else if (token.type == LEX_BOOL) {
        char name[10];
        sprintf(name, "%d", _localVariable);
        _localVariable++;
        SHTableItem *item = htableLookup(_map->tables[table], name);
        item->data.type = BOOL;
        if (strcmp("true", token.data.string) == 0) {
            item->data.data.boolean = true;
        } else {
            item->data.data.boolean = false;
        }
        stringDestroy(&token.data);
        token.type = LEX_IDENT;
        token.data = stringInit(name);
    } else if (token.type == LEX_NULL) {
        char name[10];
        sprintf(name, "%d", _localVariable);
        _localVariable++;
        SHTableItem *item = htableLookup(_map->tables[table], name);
        item->data.type = NIL;
        token.type = LEX_IDENT;
        token.data = stringInit(name);
    }
#ifdef DEBUG_TOKENS
    if (token.type == LEX_IDENT) {
#ifdef UNIX_COLORS
        printf("\033[36m");
#endif
        printf("Converting to symbol %s\n", token.data.string);
#ifdef UNIX_COLORS
        printf("\033[39m");
#endif
    }
#endif
    return token;
}

int precAnalytics(SToken *inputResulToken, SASSItem ** resultTree, int table, bool isExpr) {
    SToken input = *inputResulToken;
    SExtenStackData top, current, current2, current3, opener;

    top.token = tokenInit(LEX_SEMICOLON, ";", 0, 0);
    top.tree = NULL;

    opener.token = tokenInit(LEX_OPENER, "<", 0, 0);
    opener.tree = NULL;

    SExtenStack* stack = NULL;
    stack = stackInit();

    stackPush(stack, top);

    do {
#ifdef DEBUG_PARSER_PREC
        printf("%s %d\n", getTokenType(input.type), input.type);
        printf("%s %d\n", getTokenType(top.token.type), top.token.type);
        printf("pravidlo %c \n", preced_table[top.token.type][input.type]);
#endif

        if (top.token.type > LEX_COMMA || input.type > LEX_COMMA)
            return ERR_SYNTAX;

        switch (preced_table[top.token.type][input.type]) {
            case '=':
                if (stack->data[stack->top - 1].token.type == LEX_BRACKET_R_L && stack->data[stack->top].token.type == NON_TERM && stack->data[stack->top - 2].token.type == LEX_OPENER) {
                    current = stackPop(stack);
                    stackPop(stack);
                    stackPop(stack);

                    stackPush(stack, current);

                } else {
                    current.token = input;
                    current.tree = NULL;
                    stackPush(stack, current);
                    
                }
                
                if(stack->data[stack->top - 1].token.type != LEX_SEMICOLON || isExpr == false){
                        input = advGetToken(table, true);
                        if(input.type == LEX_UNDEFINED)
                            return ERR_NOT_DECLARED;
                }

                
                break;
            case '<':
                stackInsertAfterFirstTerm(stack, opener);
                current.tree = NULL;
                current.token = input;
                stackPush(stack, current);

                input = advGetToken(table, true);
                if(input.type == LEX_UNDEFINED)
                        return ERR_NOT_DECLARED;
                break;
            case '>':
                if (stack->data[stack->top].token.type == LEX_IDENT && stack->data[stack->top - 1].token.type == LEX_OPENER) {
                    current = stackPop(stack);
                    stackPop(stack);
                    current.tree = createLeaf(current.token);
                    current.token.type = NON_TERM;
                    stackPush(stack, current);


                } else if (stack->data[stack->top - 1].token.type <= LEX_NOT_EQUAL && stack->data[stack->top - 3].token.type == LEX_OPENER) {
                    current = stackPop(stack);
                    current2 = stackPop(stack);
                    current3 = stackPop(stack);
                    stackPop(stack);

                    current.tree = createTree(current2.token, current3.tree, current.tree);
                    current.token.type = NON_TERM;
                    stackPush(stack, current);

                } else if (stack->data[stack->top - 1].token.type == LEX_BRACKET_R_L && stack->data[stack->top].token.type == NON_TERM && stack->data[stack->top - 2].token.type == LEX_OPENER) {
                    current = stackPop(stack);
                    stackPop(stack);
                    stackPop(stack);

                    stackPush(stack, current);

                } else {
                    return ERR_SYNTAX;
                }
                break;
            default:
                return ERR_SYNTAX;
                break;
        }

#ifdef DEBUG_PREC
        stackPrint(stack);
#endif


        top = stackGetFirstTerm(stack);

    } while (!(top.token.type == LEX_SEMICOLON && (input.type == LEX_SEMICOLON || (input.type == LEX_BRACKET_R_R && isExpr == true) || (input.type == LEX_COMMA && isExpr == true))));

    top = stackPop(stack);

    if (stack->top != 0)
        return ERR_SYNTAX;

    inputResulToken->type = input.type;
    inputResulToken->data = input.data;
    inputResulToken->row = input.row;
    inputResulToken->column = input.column;

    *resultTree = top.tree;

    tokenDestroy(opener.token);
    stackDestroy(stack);
    return 0;
}