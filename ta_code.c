/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "ta_code.h"
#include <stdlib.h>
#include "error_codes.h"

#define TacBlockList 256  
#define TacBlockTable 64 

    STacItem STacCreateItem(int name, SString destination, SString paramF, SString paramS, int label){
	STacItem item;
	
	item.name = name;
	item.destination = destination;
	item.paramF = paramF;
	item.paramS = paramS;
	item.label = label;
	
	return item;
    }

    STacList STacListInit(){
	STacList list;
	//list = (STacList*) malloc(sizeof(STacList));
	//if (list == NULL)
	//    return NULL;
	
	list.item = (STacItem*) malloc(sizeof(STacItem) * TacBlockList);
	if (list.item == NULL){
	    list.size = -1;
	    return list;
	}
	list.size = 0;
	return list;
    }

    STacTable STacTableInit(){
	STacTable table;
	
	//table = (STacTable*) malloc(sizeof(STacTable));
	//if (table == NULL)
	//    return NULL;
	
	table.list = (STacList*) malloc(sizeof(STacList) * TacBlockTable);
	if (table.list == NULL){
	    table.size = -1;
	    return table;
	}
	table.size = 0;
	
	return table;
    }


    void STacListDestroy(STacList *list){
        stringDestroy(&(list->item->destination));
        stringDestroy(&(list->item->paramF));
        stringDestroy(&(list->item->paramS));
        
	free(list->item);
	//free(list);
	list = NULL;
    }

    void STacTableDestroy(STacTable *table){
	for(int i = 0; i < table->size; i++){
	    STacListDestroy(&table->list[i]);
	}
      free(table->list);
      //free(table);
      table = NULL;
    }

    int STacItemInsert(STacList *list, STacItem item){
      
      if(list->size == -1){
	    //free(table);
	    return ERR_INTERNAL;
      }
      
      if(((list->size) % TacBlockList) == 0){
	  list->item = (STacItem*) realloc(list->item, sizeof(STacItem) * TacBlockList * (list->size / TacBlockList + 1));
	  if(list->item == NULL){
	    //free(list);
	    return ERR_INTERNAL;
	  }
      }
      
      list->item[list->size] = item;
      (list->size)++;
      return 0; 
    }
    
    int STacListInsert(STacTable *table, STacList list){
      if(table->size == -1){
	    //free(table);
	    return ERR_INTERNAL;
      }
      
      if(((table->size) % TacBlockTable) == 0){
	  table->list = (STacList*) realloc(table->list, sizeof(STacList) * TacBlockTable * (table->size / TacBlockTable + 1));
	  if(table->list == NULL){
	    //free(table);
	    return ERR_INTERNAL;
	  }
      }
      
      table->list[table->size] = list;
      (table->size)++;       
      
      return 0; 
    }
    
    int STacFindLabel(STacList *list, int label, int actualItem){
	for(int i = actualItem; i < list->size ; i++){
	    if( list->item[i].label == label )
	      return i;
	      
	}
      
	for(int i = actualItem; i >= 0; i--){
	    if( list->item[i].label == label )
	      return i;      
	}
      
      return -1;
    }

    int STacPrintTable(STacTable *table){
        
        for(int i = 0; i < table->size; i++){
            printf("\nfunction %d\n", i);
            for(int p = 0; p < table->list[i].size; p ++){
                printf("%d:\t%s\t%s\t%s\t%s\n", table->list[i].item[p].label, STacGetItemName(table->list[i].item[p].name), table->list[i].item[p].destination.string, table->list[i].item[p].paramF.string, table->list[i].item[p].paramS.string);
            }
        }
        
        
        return 0;
    }
    
    char* STacGetItemName(int name){
        switch (name){
            case tac_mul: 
                return "MUL";
                break;
            case tac_div: 
                return "DIV";
                break;
            case tac_add: 
                return "ADD";
                break;
            case tac_sub: 
                return "SUB";
                break;
            case tac_con: 
                return "CON";
                break;
            case tac_ls: 
                return "LS";
                break;
            case tac_gr: 
                return "GR";
                break;
            case tac_lse: 
                return "LSE";
                break;
            case tac_gre: 
                return "GRE";
                break;
            case tac_eq: 
                return "EQ";
                break;
            case tac_neq: 
                return "NEQ";
                break;
            case tac_push: 
                return "PUSH";
                break;
            case tac_call: 
                return "CALL";
                break;
            case tac_pop: 
                return "POP"; 
                break;
            case tac_ret: 
                return "RET"; 
                break;
            case tac_goto: 
                return "GOTO"; 
                break;
            case tac_if: 
                return "IF"; 
                break;
            case tac_mov: 
                return "MOV"; 
                break;
            case tac_label: 
                return "LABEL"; 
                break;                
            default:
                return "UNKNOWN"; 
                break;
        }                
    }