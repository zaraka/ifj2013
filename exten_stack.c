/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */
#include "exten_stack.h"
#include <stdlib.h>
#include <stdio.h>

#define sizeOfBlocks 128

void stackPrint( SExtenStack* ptrstack) {
	int maxi = ptrstack->top;

	printf ("--- BOTTOM [ ");
	for ( int i=0; i<=maxi; i++ )
		printf("%s ", getTokenType(ptrstack->data[i].token.type));
	printf (" ] TOP ---\n");
}

SExtenStack* stackInit(){
    SExtenStack *stack = (SExtenStack*) malloc(sizeof(SExtenStack));
    if(stack == NULL)
      return NULL;
    stack->data = (SExtenStackData*) malloc(sizeof(SExtenStackData) * sizeOfBlocks);
    if(stack->data == NULL){
      free(stack);
      return NULL;
    }
    stack->top = -1;
    return stack;
}

void stackDestroy(SExtenStack* stack){
    while(stack->top != -1){
        //stringDestroy(&(stack->data[stack->top].token.data));
        (stack->top)--;
    }
    
    free(stack->data);
    free(stack);
    stack = NULL;
}

void stackPush(SExtenStack* stack, SExtenStackData token){
    stack->top++;
    if(stack->top % (sizeOfBlocks) == 0){
	stack->data = (SExtenStackData*) realloc(stack->data, sizeof(SExtenStackData) * sizeOfBlocks * (1 + (stack->top / sizeOfBlocks)));
	if(stack->data == NULL){
	free(stack);
	return;
	}
    }
    
    stack->data[stack->top] = token;
}

SExtenStackData stackTop(SExtenStack* stack){
    SExtenStackData error;
    error.top = -2;
    
    if(stack->top == -1){
      return error;
    }
      
    return (stack->data[stack->top]);
}

SExtenStackData stackPop(SExtenStack* stack){
    SExtenStackData error;
    error.top = -2;
    if(stack->top == -1){
      return error;
    }
    
    return (stack->data[(stack->top)--]);
}


SExtenStackData stackGetFirstTerm(SExtenStack* stack){
    SExtenStackData error;
    error.top = -2;
    if (stack->top == -1){
      return error;
    }
    
    int current = stack->top;
    
    while(stack->data[current].token.type == NON_TERM || stack->data[current].token.type == LEX_OPENER){
      current--;
      if(current < 0){
	return error;	
      }
    }
    
    return (stack->data[current]);
}

void stackInsertAfterFirstTerm(SExtenStack* stack, SExtenStackData token){

    SExtenStackData pom;
    
    int current = stack->top;
    
    stackPush(stack, token);
    
    while(stack->data[current].token.type == NON_TERM || stack->data[current].token.type == LEX_OPENER){
      pom = stack->data[current];
      stack->data[current] = stack->data[current + 1];
      stack->data[current + 1] = pom;
      current--;
      if(current < 0){
	return ;	
      }
    }
    
}
