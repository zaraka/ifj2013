/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdio.h>
#include <stdlib.h>

#include "extenStack.h"
#include "symbol_table.h"

int main(){
    SExtenStack *ptrstack = NULL;
    SExtenStackData token;
    char *a = "A";
    SString c = stringInit(a);
    token.token.data = c;
    
    printf("test1 extenstack  = initstack -> printstack -> stackdestroy\n");   
    ptrstack = stackInit();
    stackPrint(ptrstack);
    stackDestroy(ptrstack);
    printf("===============================================\n");

    printf("test2 extenstack = initstack -> stackpush -> stackprint -> stacktop -> stackprint -> stackdestroy\n"); 
    ptrstack = stackInit();
    stackPush(ptrstack, token);
    stackPrint(ptrstack);
    token = stackTop(ptrstack);
    printf("%d\n",token.top);
    stackPrint(ptrstack);
    stackDestroy(ptrstack);
    printf("===============================================\n");
    
    printf("test3 extenstack = initstack -> stackpush -> stackprint -> stackpop -> stackprint"
            "-> print -> push2x -> firstitem -> print -> destroy\n");
    ptrstack = stackInit();
    stackPush(ptrstack, token);
    stackPrint(ptrstack);
    token = stackPop(ptrstack);
    stackPrint(ptrstack);
    printf("%d\n",token.top);
    stackPush(ptrstack, token);
    stackPush(ptrstack, token);
    token= stackGetFirstTerm(ptrstack);
    printf("%d\n",token.top);
    stackPrint(ptrstack);
    stackDestroy(ptrstack);
    printf("===============================================\n");
    
    printf("test4 symboltable = initstack -> print -> destroy\n");
    STable *table = NULL;
    table = sTableInit();
    //printf("%d\n",table->tables->htable_size);
    sTableFlush(table);
    printf("===============================================\n");
    
    printf("test5 symboltable = initstack -> print -> push -> print-> pop -> print-> destroy\n");
    
    table = sTableInit();
    //printf("%d\n",table->tables->htable_size);
    int i = sTablePush(table);
    printf("%d\n",i);
    //printf("%d\n",table->tables->htable_size);
    sTablePop(table);
    //printf("%d\n",table->tables->htable_size);
    sTableFlush(table);
    printf("===============================================\n");
    
    printf("test6 symboltable = initstack -> print -> push -> push ->print -> top-> print -> destroy\n");
    
    table = sTableInit();
    //printf("%d\n",table->tables->htable_size);
    i = sTablePush(table);
    printf("%d\n",i);
    i = sTablePush(table);
    printf("%d\n",i);
    //printf("%d\n",table->tables->htable_size);
    SHashtTable* hast = sTableTop(table);
    printf("%d\n",hast->htable_size);
    //printf("%d\n",table->tables->htable_size);
    sTableFlush(table);
    printf("===============================================\n");  
            
    return 0;
}
