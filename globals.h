/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef GLOBALS_H
#define	GLOBALS_H

#ifdef	__cplusplus
extern "C" {
#endif

    /**
     * DEBUG variables.. uncomment them for more output
     */
    //#define DEBUG_CONTROLLER    
    //#define DEBUG_SCANNER
    //#define DEBUG_PARSER_PREC
    //#define DEBUG_PARSER_RULES
    //#define DEBUG_PARSER_ASS2TAC
    //#define DEBUG_TOKENS
    //#define DEBUG_INTERPRET
    //#define DEBUG_STRING
    //#define DEBUG_TREE
    //#define UNIX_COLORS

#include "symbol_table.h"

    //I Know this is horrible!!! I'm sorry
    extern SHashtTable *_functionsTable;
    extern SMapTable *_map;
    extern int _localVariable;

    int initializeGlobals();
    void freeGlobals();


#ifdef	__cplusplus
}
#endif

#endif	/* GLOBALS_H */

