/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include <stdio.h>
#include <stdlib.h>
#include "sstring.h"
#include "ial.h"

#define father 10
#define son 5
#define lenght 5
#define actlenght 10

void testSstrlen(SString string);
void testAddChar(SString *string);
void testRemoveChar(SString *string);
SString testGetString();
void testGetSubstring(SString string);
void testPutString(SString string, SString look,SString get);
void testBoyerMoore(SString string, SString look);
void testHeapSort(SString string);
int myCompare(const void * a, const void * b);

int main() {
    SString string = stringInit("zaba");
    SString look = stringInit("aba");
    SString get;
    testSstrlen(string);
    testAddChar(&string);
    testRemoveChar(&string);
    testGetSubstring(string);
    get=testGetString();
    testPutString(string, look, get);
    testBoyerMoore(string, look);
    testHeapSort(string);
    return 0;
}

void testSstrlen(SString string) {
    printf("SSTRLEN:\n");
    if ((stringStrlen(string) == sstrlen(string.string)) && (strlen(string.string) == sstrlen(string.string))) {
        printf("OK\n\n");
    } else {
        printf("FAIL\n\n");
    }
}

void testAddChar(SString *string) {
    printf("ADDCHAR:\n");
    SString test = stringInit("zabak");
    if (stringAddChar(string, 'k') == 0) {
        if ((strcmp(test.string, string->string) == 0) && (string->stringSize == test.stringSize)) {
            printf("OK\n\n");
        } else {
            printf("FAIL\n\n");
        }
    }
    stringDestroy(&test);
}

void testRemoveChar(SString *string) {
    printf("REMOVECHAR:\n");
    SString test = stringInit("zaba");
    if (stringRemoveChar(string) == 0) {
        if ((strcmp(test.string, string->string) == 0) && (string->stringSize == test.stringSize)) {
            printf("OK\n\n");
        } else {
            printf("FAIL\n\n");
        }
    }
    stringDestroy(&test);
}

SString testGetString() {
    printf("GETSTRING:\n"); 
    int i;
    SString string=get_string(&i);
    if(i==0){
        printf("%s\n\n",string.string);
    }
    return string;    
}

void testGetSubstring(SString string) {
    printf("SUBSTRING:\n");
    SString out = stringInit("");
    if (get_substring(string, &out, 0, 4) == 0) {
        if ((strcmp(out.string, "zaba") == 0)&&(out.stringSize==strlen(out.string))){            
            printf("OK\n\n");
        } else {
            printf("FAIL\n\n");
        }
    }
    stringDestroy(&out);
}

void testPutString(SString string, SString look,SString get) {
    printf("PUTSTRING:\n");
    int i = put_string(string, look, get, NULL);
    printf("\n");
    int j = put_string(string, look, NULL);
    printf("\n");
    if (i == 3 && j == 2) {
        printf("OK\n\n");
    } else {
        printf("FAIL\n\n");
    }
}

void testBoyerMoore(SString string, SString look) {
    printf("VYHLEDAVANI:\n");
    int s = find_string(string, look);
    if (s>-1) {
        printf("retezec nalezen na: %d. pozici\n\n", s);
    } else {
        printf("retezec nenalezen\n\n");
    }
}

void testHeapSort(SString string) {
    printf("RAZENI:\n");
    char* qstr = (char*) malloc(10 * (string.stringSize / 10 + 1));
    strcpy(qstr, string.string);
    string = sort_string(string);
    qsort(qstr, string.stringSize, sizeof (qstr[0]), myCompare);
    if (strcmp(qstr, string.string) == 0) {
        printf("OK\n");
    } else {
        printf("FAIL\n");
    }
    free(qstr);
}

int myCompare(const void * a, const void * b) {
    const char pa = *(const char*) a;
    const char pb = *(const char*) b;
    return pa - pb;
}
