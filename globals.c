/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#include "globals.h"
#include "parser.h"
#include "error_codes.h"

SHashtTable *_functionsTable;
SMapTable *_map;
int _localVariable;

int initializeGlobals() {
    _functionsTable = htableInit(HASH_TABLE_SIZE);
    if (_functionsTable == NULL) {
        destroyScanner();
        return ERR_INTERNAL;
    }
    _map = initSMap();
    if (_map == NULL) {
        htableFree(_functionsTable);
        destroyScanner();
        return ERR_INTERNAL;
    }
    
    _localVariable = 0;

    return 0;
}

void freeGlobals() {
    htableFree(_functionsTable);
    _functionsTable = NULL;
    destroySMap(_map);
    _map = NULL;
}