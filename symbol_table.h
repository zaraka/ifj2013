/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef SYMBOL_TABLE_H
#define	SYMBOL_TABLE_H

#include <stdlib.h>

#include "ial.h"

#ifdef	__cplusplus
extern "C" {
#endif


    /**
     * Stack of symbol tables for interpret
     */
    typedef struct STable{
        SHashtTable **tables;
        int top;
    } STable;

    /**
     * Map Table of symbol tables
     */
    typedef struct SMapTable {
        SHashtTable **tables;
        int size;
    } SMapTable;

    SMapTable* initSMap();
    int insertSMap(SMapTable* map);
    void destroySMap(SMapTable* map);
    void printSMap(SMapTable* map);
    /**
     * Will initialize Symbol Table
     * @return new symbol table
     */
    STable* sTableInit();

    /**
     * Push new hashTable into Symbol Table
     * @param table
     * @return 
     */
    int sTablePush(STable *sTable);

    /**
     * Will return top table in stack
     * @param table
     * @return top hash table
     */
    SHashtTable* sTableTop(STable *sTable);

    /**
     * Will destroy top item in stack
     * @param table
     */
    void sTablePop(STable *sTable);

    /**
     * Will destroy all items in stack
     * This should be called as destryoing symbol table
     * @param table
     */
    void sTableFlush(STable *sTable);



#ifdef	__cplusplus
}
#endif

#endif	/* SYMBOL_TABLE_H */

