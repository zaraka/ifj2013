/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef PARSER_H
#define	PARSER_H

#include "scanner.h"
#include "exten_stack.h"
#include "ial.h"
#include "globals.h"

#ifdef	__cplusplus
extern "C" {
#endif

    /**
     * UNUSED at the moment
     */
    enum ASS_TYPE {
        LEAF,
        TREE
    };

    typedef struct SASSItem SASSItem;

    struct SASSItem {
        SToken token;
        int type;
        SASSItem *lItem;
        SASSItem *rItem;
    };

    /**
     * Container for a tree
     */
    typedef struct SASS {
        SASSItem *root;
    } SASS;

    /**
     * Will create Leaf for ASS tree
     * @param token
     * @return 
     */
    SASSItem* createLeaf(SToken token);

    /**
     * Will create Tree for ASS tree
     * @param token
     * @param left
     * @param right
     * @return 
     */
    SASSItem* createTree(SToken token, SASSItem* left, SASSItem* right);

    void destroyTree(SASSItem* root);
    
    /**
     * Destroy and frees ASS tree
     * @param root
     */
    void destroyASS(SASSItem* root);

    /**
     * 
     * @param inputResulToken
     * @param resultTree
     * @param table
     * @return 
     */
    int precAnalytics(SToken *inputResulToken, SASSItem **resultTree, int table, bool isExpr);

    /**
     * will create Abstract Syntax Symbol tree
     * @param filename
     * @return 
     */
    int getASS(const char* filename, SASS *tree);

    void printASS(SASSItem* item);
    
    /**
     * Wrapper for getToken() 
     * THIS SHOULD BE CALLED INSTEAD OF getToken()
     * This will also convert INT, FLOAT and STRING
     * to IDENT
     * @return 
     */
    SToken advGetToken(int table, bool isExpr);

    /**
     * Well I don't really use this
     */
    enum NON_TERM {
        START,
        ST_LIST,
        STAT,
        IF_LIST,
        FUNC_LIST,
        ID_LIST,
        ASSIGN_LIST,
        IDID_LIST
    };

#ifdef	__cplusplus
}
#endif

#endif	/* PARSER_H */

