/**
 * Projekt do předmětu IFJ překladač jazyka IFJ13
 * Členové týmu
 * xvanku00:20
 * xkrato35:20
 * xkucha21:20
 * xzadan00:20
 * xkrain02:20
 */

#ifndef SSTRING_H
#define	SSTRING_H

#ifdef	__cplusplus
extern "C" {
#endif

    typedef struct SString{
        char* string;
        int stringSize;
    } SString;

    SString stringInit(char* str);
        
    void stringDestroy(SString *string);
        
    int stringAddChar(SString *string, char ch);
        
    int stringRemoveChar(SString *string);
    
    SString get_string(int *err_code);
    
    int put_string(SString string, ...);
    
    int stringStrlen(SString string);
    
    int get_substring(SString string, SString *outString, int start, int stop);
    
    int compare_strings(SString fir,SString sec);
    
    SString concate_string(SString fir,SString sec);
    

#ifdef	__cplusplus
}
#endif

#endif	/* SSTRING_H */

